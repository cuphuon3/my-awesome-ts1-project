package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Cuphuon3Test{
    @Test
    public void factorialTest() throws Exception{
        Cuphuon3 myClass = new Cuphuon3();
        long expectedResult = 120;
        long result = myClass.factorial(5);
        try{
            assertEquals(expectedResult, result);
        } catch (Exception e) {
            System.out.println("Zachycena vyjimak");
        }
    }
}
