package cz.cvut.fel.ts1;

public class Durisja4 {
    public long factorial(int n) {
        if (n < 0) {
            throw new RuntimeException("Nelze vypocitat faktorial zaporneho cisla.");
        } else if (n <= 1) {
            return 1;
        }
        return n * factorial(n-1);
    }
}
