<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Price List</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
</head>

<body class="priceListPage">
    <header>    
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main >
        <div class="priceList">
            <span class="Price">PRICE </span><span class="getUnderline">List</span>
        </div>

        <div class="description">
            <h3>Basic cut <span class="pricing">500Kc</span> </h3>
            <p>Washing, haircut with clipper and scissors, styling</p>

            <h3>Beard adjustment <span class="pricing">400Kc</span> </h3>
            <p>Trimming, hot and cold towel, oil treatment, after shave, styling</p>

            <h3>Kid's <span class="pricing">350Kc</span> </h3>
            <p>Haircut for children age 10 and under</p>

            <h3>Buzz cut all over <span class="pricing">350Kc</span> </h3>
            <p>Haircut with machine only</p>

            <h3>Premium package <span class="pricing">1300Kc</span> </h3>
            <p>Consultation, 2 washes, classic hair cut, bread trim, </p>
            <p class="undertext">eyebrow styling black mask, massage hair tonic, styling and perfume</p>
        </div>
    </main>
    <?php
    include("../include/Footer.php")
    ?>
</body>

</html>