<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gallery</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
    <script src="../static/js/gallery.js"></script>
</head>

<body class="galleryPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    
    <main>
        <div class="Gallery">
            <span class="getBold">GALLERY</span>
        </div>

        <div class="pictures">
            <img src="../static/img/gallery1.jpg" alt="Beard adjustment" onclick="myFunction(this);">
            <img src="../static/img/gallery2.jpg" alt="Hair Styling" onclick="myFunction(this);">
            <img src="../static/img/gallery3.jpg" alt="Mid Fade" onclick="myFunction(this);">
            <img src="../static/img/gallery4.jpg" alt="Kids Cut" onclick="myFunction(this);">
            <img src="../static/img/gallery5.jpg" alt="Basic Cut" onclick="myFunction(this);">
            <img src="../static/img/gallery7.jpg" alt="Line Draw" onclick="myFunction(this);">
        </div>

        <div class="container">
            <img id="zoomIn" src="#" alt="choosedPicture">
        </div>
    </main>

    <?php
    include("../include/Footer.php")
    ?>
</body>
</html>