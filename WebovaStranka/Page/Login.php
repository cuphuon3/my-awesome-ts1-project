<?php
  session_start();
  include("../DB/CreateConnection.php");
  include("../DB/GetCustomer.php");
  //vytvářím spojení s databází a getdata od zákazníka

  if (isset($verify)) {
    header("Location: Booked.php");
    //pomocí verify, zjistuji, jestli uzivatel zadal správně email a heslo
    //pokud uživatel je již přihlášený a klikne na my reservation, tak se nemusí znova přihlasovat, ale verify již vraci true
  }

  $isForm = count($_POST) > 0;
  //isform vraci true když se odesle vyplnený formulár
  include '../validate/Validate.php';

  $emailValue = '';
  $PasswordValue = '';

  if ($isForm) {
    $emailValue = $_POST["email"];
    $PasswordValue = $_POST["password"];

    $validEmail = validateEmail($emailValue);
    $validPassword = validatePassword($PasswordValue);
    //overovani spravne validnosti hodnot jednotlivych inputu
    //spravne zapsane hodnoty vraci TRUE, spatne vyplnene FALSE
    //funkce jsou v knihovne validate.php
    
    $valid = $validEmail && $validPassword;
    //vrací true, pokud všechny promenné jsou true

    if ($valid) {
      $_SESSION['email'] = $_POST["email"];
      include '../DB/CreateConnection.php';
      //spojim se s databazi jen pro kontrolu funkcnosti serveru
      include '../DB/GetCustomer.php';
      // getdata zákazníka
      include '../DB/Verify.php';
      // kontrola vyplneni spravneho emailu a hesla
    }
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Log IN</title>
  <link rel="stylesheet" media="screen" href="../static/css/style.css">
  <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
  <script src="../static/js/form.js"></script>
</head>

<body class="loginPage">
  <header>
        <?php
            include("../include/Navigation.php");
        ?>
  </header>
  <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
  <main>
    <div class="LogIn">
      <span class="Log">LOG </span><span class="getUnderline">IN</span>
    </div>

    <div class="Booking">
      <form method="POST">

        <label for="email">
          <legend>Email</legend>
          <input type="text" id="email" name="email" placeholder="Your email.." value="<?php echo htmlspecialchars($emailValue)?>" pattern="[a-z-A-Z0-9._%+-]+@[a-z-A-Z0-9.-]+\.[a-z-A-Z]{2,}$" required title="Example: milan@seznam.cz">
          <?php
            if(isset($validEmail) && $validEmail == false){
              echo "<span class='errorInput'>Invalid email format</span>";
            }
          ?>
        </label>

        <label for="password">
          <legend>Password</legend>
          <input type="password" id="password" name="password" placeholder="Your password.." pattern=".{5,}" required >
          <?php
            if(isset($validPassword) && $validPassword == false){
              echo "<span class='errorInput'>Wrong Password</span>";
            }
          ?>
        </label>
        <input id="submit" type="submit" value="LOG IN">
        <p class='errorInput'>
          <?php
            echo isset($error) ? $error : '';
          ?>
        </p>
      </form>
      
      <form action="ChooseDay.php" method="POST">
        <input id="registration" type="submit" value="REGISTRATION">
      </form>

      <script>
        init();
      </script>

    </div>
  </main>


  <?php
    include("../include/Footer.php")
  ?>
</body>

</html>

