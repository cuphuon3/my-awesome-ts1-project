<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Please Log In</title>
  <link rel="stylesheet" media="screen" href="../static/css/style.css">
  <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="DuplicityPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <div class="Alert">
            <span class="getBold">You already have a reservation </span><span class="getUnderline"> PLEASE LOG IN</span>
        </div>

        <div class="midButton">
            <a href="Login.php" class="login">Log In</a>
        </div>
    </main>

    <?php
        include("../include/Footer.php")
    ?>
</body>

</html>