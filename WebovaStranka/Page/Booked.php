<?php
    session_start();
    include("../DB/CreateConnection.php");
    include("../DB/GetCustomer.php");
    include("../DB/LoggedCheck.php");
    // includuju knihovny pro spojení a getData zákazníka, které pak echuju
    // LoggedCheck slouží k tomu, aby se ujsitil, že je uživatel přihlášený, pokud ne, tak se stránka přesměruje na login page.
    if ($customerEmail == "admin@zwabarber.shop"){
        //pokud je uživatel přihlášený emailem administrátora, stránka se přesměruje na allreservation.php
        header('Location: AllReservation.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Booked</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">

</head>
<body class="bookedPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main class="YourReservation">
        <span class="Your">YOUR </span><span class="getUnderline">RESERVATION</span>
    </main>
    
    <div class="container">
        <div id="day">
            <h2>Reserved day and time</h2>
            <p>
                <?php
                    echo htmlspecialchars ($reseredDay);
                ?>
            </p>
        </div>

        <div id="name">
            <h2>Your name</h2>
            <p>
                <?php
                    echo htmlspecialchars($customerName);
                ?>
            </p>
        </div>
        
        <div id="email">
            <h2>Your email</h2>
            <p>
                <?php
                    echo htmlspecialchars ($customerEmail);
                ?>
            </p>
        </div>

        <div id="telnum">
            <h2>Your telephone number</h2>
            <p>
                <?php
                    echo htmlspecialchars ($customerTel);
                ?>
            </p>
        </div>
        <a href="../DB/EditReservation.php" class="EditkButton">EDIT</a>
        <a href="../DB/DeletefromDB.php" class="DeleteButton">DELETE</a>
        <a href="../DB/LogOut.php" class="LogOut">LOG OUT</a>
    </div>

    <?php
        include("../include/Footer.php")
    ?>
</body>
</html>