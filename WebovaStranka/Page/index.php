<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Barber Shop</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
</head>
<body class="homePage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <h2>
            <span class="ZWA">ZWA</span>
            <span class="getUnderline">BARBER SHOP</span>
        </h2>
        <p>
            We are not just a shop, we are a <span class="getBold">brand</span>
        </p>
    </main>

    <div class="midButton">
        <a href="ChooseDay.php" class="reservation">Make a Reservation</a>
    </div>

    <?php
        include("../include/Footer.php")
    ?>
</body>

</html>