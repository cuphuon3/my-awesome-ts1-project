<?php
    session_start();
    include ('../DB/CreateConnection.php');
    if (!isset($_SESSION['day'])) {
        header("Location: ChooseDay.php");
        //pokud uživatel nevybral den, tak se přesměruje na chooseday.php
    }

    include("../DB/GetTime.php");
    // knihovna gettime nám vrací dny, které jsou již rezervované.
    include ('../validate/Validate.php');
    // knihovna inklude nám validuje proměnné
    $isForm = ($_SERVER["REQUEST_METHOD"] == "POST");
    //isform varcí true, pokud se odeslal formulář

    $nameValue = '';
    $emailValue = '';
    $unHashPass = '';
    $passwordValue = '';
    $passwordCheck = '';
    $telnumberValue = '';
    $dayValue = '';
    $timeValue = '';
    $textValue = '';
    $covidValue ='';
    // definuji si proměnné pro value jednotlivých inputů

    if ($isForm) {
        $idValue = uniqid();
        //přiděluju unikátní ID
        $nameValue = htmlspecialchars($_POST["name"]);
        $emailValue = htmlspecialchars($_POST["email"]) ;
        $unHashPass = htmlspecialchars($_POST["password"]);
        $passwordCheck = htmlspecialchars($_POST["passwordCheck"]);
        $passwordValue = htmlspecialchars(password_hash($_POST["password"], PASSWORD_DEFAULT));
        //solím heslo pomocí password hash
        $telnumberValue = htmlspecialchars($_POST["telnumber"]);
        $dayValue = htmlspecialchars($_SESSION['day']);
        $covidValue = htmlspecialchars($_POST["covid"]);
        $textValue = ($_POST['text']);
        if(isset($_POST["time"])){
            $timeValue = htmlspecialchars($_POST["time"]);
        } else {
            $timeValue = '';
        }

        $validName = validateName($nameValue);
        $validTime = validateTime($timeValue);
        $validEmail = validateEmail($emailValue);
        $validTelNumber = validateTelNumber($telnumberValue);
        $validPassword = validatePassword($unHashPass);
        $validPasswordCheck = validatePasswordCheck($passwordCheck, $unHashPass);
        $validCovid = validateCovid($covidValue);
        //overovani spravne validnosti hodnot jednotlivych inputu
		//spravne zapsane hodnoty vraci TRUE, spatne vyplnene FALSE

        $valid = $validName[1] && $validTime[1] && $validEmail[1] && $validTelNumber[1] && $validPassword[1] && $validPasswordCheck[1] && $validCovid[1];
        //funkce valid vrací true, pokud jsou všechna validace validní
        if ($valid) {
            $_SESSION['email'] = $_POST["email"];
            include ('../DB/CheckDuplicity.php');
            // knihovny checkduplicity nam zjistuje, jestli dany email uz nema registraci
            if (checkDuplicity($_POST["email"])){
                //pokud check duplicity vrati true, uzivatel se muze zaregistrovata
                include ('../DB/CreateConnection.php');
                include ("../DB/InsertReservation.php");
                //vytváří se spojení s databází a vloží se data do databáze
            } else {
                // pokud check duplicity vrati false
                // stránka smaže sešny a přesměruje se na duplicity alert.
                $_SESSION['id'] = null;
                $_SESSION['email'] = null;
                header('Location: DuplicityAlert.php');
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book IT</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="../static/js/bookIt.js"></script>
</head>

<body class="bookItPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <div class="BookIT">
            <span class="Book">BOOK </span><span class="getUnderline">IT</span>
        </div>

        <div class="form">
            <legend id="Choosedday">
                <?php
                    echo htmlspecialchars ($_SESSION['day']);
                    // vypisuju zde datum, který si zákazník na předchozím formuláři vybral
                ?>
            </legend>
            <form method="POST">
                <fieldset id="Time">
                    <div class="timeArticle">
                        <legend>*Select time</legend>
                        <label for="nine"><input type="radio" value="9:00:00" name="time" id="nine" required title="Choose from free time" <?php if (in_array("9:00:00", $freeTime)){
                        echo "disabled";}?>  <?php if($timeValue == '9:00:00'){echo 'checked';}?> />9:00</label>

                        <label for="ten"><input type="radio" value="10:00:00" id="ten" name="time" <?php if($timeValue == '10:00:00'){echo 'checked';}?> <?php if (in_array("10:00:00", $freeTime)){
                echo "disabled";
            } ?>/>10:00</label>
                        <label for="eleven"><input type="radio" value="11:00:00" id="eleven" name="time" <?php if($timeValue == '11:00:00'){echo 'checked';}?> <?php if (in_array("11:00:00", $freeTime)){
                echo "disabled";
            } ?> />11:00</label>
                        <label for="twelve"><input type="radio" value="12:00:00" id="twelve" name="time" <?php if($timeValue == '12:00:00'){echo 'checked';}?> <?php if (in_array("12:00:00", $freeTime)){
                echo "disabled";
            } ?> />12:00</label>
                        <label for="thirteen"><input type="radio" value="13:00:00" id="thirteen" name="time" <?php if($timeValue == '13:00:00'){echo 'checked';}?> <?php if (in_array("13:00:00", $freeTime)){
                echo "disabled";
            } ?> />13:00</label>
                        <label for="fourteen"><input type="radio" value="14:00:00" id="fourteen" name="time" <?php if($timeValue == '14:00:00'){echo 'checked';}?> <?php if (in_array("14:00:00", $freeTime)){
                echo "disabled";
            } ?> />14:00</label>
                        <label for="fifteen"><input type="radio" value="15:00:00" id="fifteen" name="time" <?php if($timeValue == '15:00:00'){echo 'checked';}?> <?php if (in_array("15:00:00", $freeTime)){
                echo "disabled";
            } ?> />15:00</label>
                        <label for="sixteen"><input type="radio" value="16:00:00" id="sixteen" name="time" <?php if($timeValue == '16:00:00'){echo 'checked';}?> <?php if (in_array("16:00:00", $freeTime)){
                echo "disabled";
            } ?> />16:00</label>
            <!-- $timevalue nám zajištuje to, aby se zadana nesmazala po neúspešném odeslání formuláře, pokud se hodnota timevalue rovná dané hodnote, tak se přidá atribut checked -->
            <!-- pole in array vrací již rezervovaná časy, pokud je daný čas v array, prida se atriut disabled a radio button nepujde vybrat -->
                        <p id="errorTime">
                            <?php
                                if(isset($validTime)){
                                    echo $validTime[0];
                                }
                                //validtime[0] nám vrací chybovou hlášku a nebo nic
                            ?>
                        </p>
                    </div>
                </fieldset>

                <fieldset id="Name">
                    <label for="CustomerName"><legend>*Type your name</legend>
                    <input type="text" name="name" id="CustomerName" pattern="[A-Za-zěĚšŠčČřŘžŽýÝáÁíÍéÉÓó ]{4,}" value="<?php echo isset($_SESSION['name']) ? $_SESSION['name'] : ''; ?>" required title="Example: Milan" placeholder="name" >
                    </label>
                    <p id="errorName">
                        <?php
                            if(isset($validName)){
                                echo $validName[0];
                                //validName[0] nám vrací chybovou hlášku a nebo nic
                            }
                        ?>
                    </p>
                </fieldset>

                <fieldset id="Email">
                    <label for="email"><legend>*Type your email</legend><input type="text" name="email" id="email"
                    placeholder="email@example.com" pattern="[a-z-A-Z0-9._%+-]+@[a-z-A-Z0-9.-]+\.[a-z-A-Z]{2,20}$" title="Example: milan@seznam.cz" value="<?php echo isset($_SESSION['email']) ? $_SESSION['email'] : ''; ?>" required ></label>
                    <p id="errorEmail">
                        <?php
                            if(isset($validEmail)){
                                echo $validEmail[0];
                                //validEmail[0] nám vrací chybovou hlášku a nebo nic
                            }
                        ?>
                    </p>
                </fieldset>

                <fieldset id="Telnumber">
                    <label for="telnumber"><legend>*Type your telephone number</legend>
                    <input type="text" name="telnumber" id="telnumber" placeholder="777888666" pattern="[0-9]{9}" value="<?php echo isset($_SESSION['TelNum']) ? $_SESSION['TelNum'] : ''; ?>" title="Example: 777888666" required></label>
                    <p id="errorTel">
                        <?php
                            if(isset($validTelNumber)){
                                echo $validTelNumber[0];
                                //validTelNumber[0] nám vrací chybovou hlášku a nebo nic

                            }
                        ?>
                    </p>
                </fieldset>
  
                <fieldset id="Password">
                    <label for="password"><legend>*Type your password</legend>
                    <input type="password" id="password" name="password" placeholder="password" pattern="[A-Za-z-0-9]{5,}" required title="Password (6 characters minimum)"></label>
                    <p id="errorPass">
                        <?php
                            if(isset($validPassword)){
                                echo $validPassword[0];
                                //validPassword[0] nám vrací chybovou hlášku a nebo nic
                            }
                        ?>
                    </p>
                </fieldset>

                <fieldset id="PasswordCheck">
                    <label for="password"><legend>*Type your password again</legend>
                    <input type="password" id="passwordCheck" name="passwordCheck" placeholder="password" required></label>
                    <p id="errorPassCheck">
                        <?php
                            if(isset($validPasswordCheck)){
                                echo $validPasswordCheck[0];
                                //validPassword[0] nám vrací chybovou hlášku a nebo nic
                            }
                        ?>
                    </p>
                </fieldset>
    
                <fieldset id="comment">
                    <label for="vzkaz"><legend>Write your message here</legend>
                        <textarea id="vzkaz" cols="40" rows="8" name="text" placeholder="text..."><?php echo htmlspecialchars($textValue) ?></textarea>
                    </label>
                </fieldset>

                <fieldset id="covidAccept">
                <label for="covid"><legend>Covid okénko</legend>
                    <div id="covid">
                        <legend>Čestné prohlášení Já, čestně prohlašuji, že: <br>
                        -se u mě neprojevují a v posledních dvou týdnech neprojevily příznaky virového infekčního<br>
                        onemocnění (např. horečka, kašel, dušnost, náhlá ztráta chuti a čichu apod.)<br>
                        -jsem nebyl/la diagnostikován/a COVID-19 pozitivní<br>
                        -mi nebyla nařízená karanténa v důsledku diagnostikovaného COVID-19 onemocnění nebo kontaktu s COVID-19 pozitivní osobou<br>
                        -jsem se v posledních dvou týdnech (vědomě) nesetkal/a s COVID-19 pozitivní osobou.<br>
                        Jsem si vědom(a) právních následků v případě, že by toto prohlášení nebylo pravdivé
                        </legend>
                        <input type="radio" id="ano" name="covid" value="ano" required title="Choose from one">
                        <label for="ano">Ano</label><br>
                        <input type="radio" id="ne" name="covid" value="ne">
                        <label for="ne">Ne</label><br>
                        <p id="errorPass">
                        <?php
                            if(isset($validCovid)){
                                echo $validCovid[0];
                            }
                        ?>
                    </p>
                    </div>
                </fieldset>

                <fieldset id="buttons">
                    <a href="ChooseDay.php" class="BackButton">BACK</a>
                    <input type="reset" value="RESET" />
                    <input type="submit" value="SEND" />
                </fieldset>

                <p class="required">* = required field</p>
            </form>
            <script>
                var timerID = null;
                init();
            </script>
        </div>
    </main>
    <?php
        include("../include/Footer.php")
    ?>
</body>

</html>