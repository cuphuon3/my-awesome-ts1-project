<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Us</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
</head>
<body class="contactUsPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <div class="ContactUs">
            <span class="Contact">CONTACT </span><span class="getUnderline">US</span>
        </div>

        <div class="windows">
            <div class="address">
                <img src="../static/img/adresa.png" alt="gps" class="icon">
                <h3>ADDRESS</h3>
                <p>
                    Karlovo náměstí 13,
                </p>
                <p>
                    121 35 Praha 2
                </p>
                <p>
                Nové Město
                </p>
            </div>

            <div class="contact">
                <img src="../static/img/telephone.png" alt="telephone" class="icon">
                <h3>CONTACT</h3>
                <p>
                    <span class="getColor">Tel.:</span> +420 777 777 777
                </p>
                <p>
                    <span class="getColor">E-mail:</span> info@zwashop.cz
                </p>
                <p>
                <a href="ChooseDay.php" id="reservation"><span class="getColor">Make a reservation &#62;</span> </a>
                </p>
            </div>

            <div class="openhours">
                <img src="../static/img/Time.png" alt="clock" class="icon">
                <h3>OPENING HOURS</h3>
                <p>
                    <span class="getColor">Monday - Friday:</span>
                </p>
                <p>
                    According to orders
                </p>
            </div>
        </div>
    </main>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2559.989840474638!2d14.40924791569418!3d50.08647707942677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470b94e5e58eb59f%3A0x75209738d1d3b126!2sKarl%C5%AFv%20most!5e0!3m2!1scs!2scz!4v1606336632799!5m2!1scs!2scz"></iframe>

    <?php
    include("../include/Footer.php")
    ?>

</body>

</html>