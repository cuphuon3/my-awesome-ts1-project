<?php
    session_start();
    include("../DB/CreateConnection.php");
    //vytvářim spojení na databázi, připojuju se už tady, kdyby náhodou vznikla chyba na databázi, tak at toho zákazníka nevyhodí až při registraci
    $isForm = count($_POST) > 0;
    //isForm vraci true pokud se odeslal formulář

    if ($isForm) {
        include('../validate/Validate.php');
        //includuju knihovnu kde se validují porměnné
        if (validateDay($_POST["day"])){
            //pokud validace vrací true
            session_destroy();
            //nicím sešn, aby se registroval pouze nepřihlášený uživatel
            session_start();
            $_SESSION['day'] = $_POST["day"];
            //zapisuju do $_SESSION['day'] den, abych mohl pak pomocí dne validovat volný čas
            header("location: BookIT.php");
            //poté se přesměruju na hlavní formulář
        } else {
            //pokud uživatel vybere špatný datum
            include('../DB/GetDay.php');
            $GetDay = getDay();
            //getDay nám vrací, nejbližší možný termín a nejedelší
            $error = "PLEASE CHOOSE DAY IN INTERVAL $GetDay[0] TO $GetDay[1]";
            // pomocí erroru pak zobrazuju chybovou hlášku
        } 
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Choose a day</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
    <script src="../static/js/date.js"></script>
</head>
<body class="chooseDayPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main class="BookIT">
        <span class="Book">BOOK </span><span class="getUnderline">IT</span>
    </main>
    <div class="container">
        <form method="POST">
            <fieldset id="Day">
                <legend>*Choose a day</legend>
                <label for="choosedDay">
                    <input type="date" id="choosedDay" name="day" min=<?php echo date('Y-m-d', strtotime(' +1 day'));?> max=<?php echo date('Y-m-d', strtotime(' +7 day'));?> value="<?php echo date('Y-m-d', strtotime(' +1 day')); ?>" required >
                </label>
                <p id="error">
                    <?php
                        echo isset($error) ? $error : '';
                        //tady se vypíše chyba
                    ?>
                </p>
            </fieldset>
            <fieldset id="buttons">
                <input type="submit" value="NEXT" />
            </fieldset>
            <p class="required">* = required field</p>
        </form>
    </div>

    <script>
        init();
    </script>

    <?php
        include("../include/Footer.php")
    ?>
</body>
</html>