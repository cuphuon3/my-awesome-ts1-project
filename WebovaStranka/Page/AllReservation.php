<?php
    session_start();
    include("../DB/CreateConnection.php");
    include("../DB/GetCustomer.php");
    //připojuju se na databázi a kontroluju jestli existuje přihlašený uživatel
    if (isset($customerEmail) != "admin@zwabarber.shop"){
        // pokud přihlášený uživatel nemá email administrátora, je přesměrován na login page
        header('Location: Login.php');
    } else {
        if(!isset($customerEmail)){
            header('Location: Login.php');
            // pokud není uživatel přihlášený, je přesměrován na login page
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Reservation</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
</head>
<body class="allReservationPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <div class="AllReservation">
            <span class="ALL">ALL </span><span class="getUnderline">Reservation</span>
        </div>
        <div class="container">
            <div class="windows">
                <?php
                    include("../DB/GetAllReservation.php");
                    //knihovna GetAllReservation vraci všechny rezervace v rozmezi den+1 až po den+8

                    $limit = 1;
                    //nastavený limit najednu stránku
                    $total = count($allreservation);
                    //celkova delka vsech rezervaci
                    if ($total == 0) {
                        //pokud neni ani jedna rezervace, vypisuju no reservation
                        echo "<div class='day'>";
                        echo 'No Comments';
                        echo "</div>";
                    }
                    if ($total > 0) {
                        //pokud je alespon jedna rezervace
                        $pages = ceil($total/$limit);
                        //vypocitam si kolik stranek budu potrebovat, jelikoz mam na kazdou stranku jednu rezervaci, bude pocet stranek stejny jako pocet rezervacis
                        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
                            'options' => array(
                                'default'   => 1,
                                'min_range' => 1,
                            ),
                        )));
                        $offset = ($page - 1) * $limit;
                        
                        $reservationAtDay = $allreservation[$offset];
                        //přiiřazuju ke reservationatday konkrétní den
                        
                        foreach ($reservationAtDay as $oneReservation) {
                            // tisknu vsechny rezervace na jednu stránku, v mém případě vždycky jedna
                            echo "<div class='day'>";
                            echo $oneReservation;
                            echo "</div>";
                        }
                        
                        if ($page > 1) {
                            $prevlink = '<a href="?page=1">&laquo;</a> <a href="?page='.($page-1).'">&lsaquo;</a>';
                            // << mě odkazuje na předchozí stranku
                        } else {
                            // pokud jsem na první sttánce, tak << je pouze jako znak
                            $prevlink = "&laquo; &lsaquo;";
                        }
                        
                        $links = "";
                        //links mi určuje počet stránek
                        for ($i=0; $i < $pages; $i++) {
                            if ($i == $page-1 ) {
                                $links .= " ".($i+1)." ";
                            } else {
                                $links .= " <a href=\"?page=".($i+1)."\">".($i+1)."</a> ";
                            }
                        }
                        
                        
                        if ($page < $pages) {
                            $nextlink = '<a href="?page='.($page+1).'">&rsaquo;</a> <a href="?page='.$pages.'">&raquo;</a>';
                            // >> mě odkazuje na následující stránku
                        } else {
                            // pokud jsem na poslední stránce, tak >> je pouze jako znak
                            $nextlink = "&rsaquo; &raquo;";
                        }

                        echo "<span class='prevlink'>$prevlink</span>", $links, "<span class='nextlink'>$nextlink</span>"; 
                        // vypisuju << pocet stránek >>
                    }
                ?>
            </div>

            <a href="../DB/LogOut.php" class="LogOut">LOG OUT</a>
        </div>
    </main>
    <?php
        include("../include/Footer.php")
    ?>
</body>
</html>