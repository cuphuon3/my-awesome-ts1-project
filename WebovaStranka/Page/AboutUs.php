<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
</head>
<body class="aboutUsPage">
    <header>
        <?php
        include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <main>
        <div class="AboutUs">
            <span class="About">ABOUT </span><span class="getUnderline">US</span>
        </div>

        <div class="description">
            <p class="article">
            We are two guys who worked in a prestigious barbershop and
            decided to turn the shop into the brand
            </p>

            <p class="article">
            We do not take our profession as a job, but as a hobby, and
            therefore we try to do our services as best we can and we
            want each of our clients to be satisfied
            </p>

            <p class="article">
            Here you can find a friendly atmosphere and pleasant service
            </p>
        </div>

        <div class="images">
            <img src="../static/img/2clen.jpg" alt="2clen">
            <img src="../static/img/3clen.jpg" alt="3clen">
        </div>
    </main>


    <?php
    include("../include/Footer.php")
    ?>
</body>

</html>