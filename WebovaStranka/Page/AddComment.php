<?php
    session_start();
    include ('../DB/CreateConnection.php');
    include("../DB/GetCustomer.php");
    include("../DB/LoggedCheck.php");
    if(!isset($customerEmail)){
        header('Location: Login.php');
        // pokud není uživatel přihlášený, je přesměrován na login page
    }
    include("../DB/getTitleOpinion.php");
    include ('../validate/Validate.php');
    $isForm = ($_SERVER["REQUEST_METHOD"] == "POST");

    $titleValue = '';
    $serviceValue = '';
    $commentValue = '';
    if ($isForm) {

        $email = $_SESSION['email'];
        $titleValue = htmlspecialchars($_POST["title"]);
        $serviceValue = htmlspecialchars($_POST["service"]) ;
        $commentValue = htmlspecialchars($_POST["text"]);
        $imgValue = htmlspecialchars($_POST["myImage"]);

        $validTitle = validateTitle($titleValue);
        $validService = validateService($serviceValue);
        $validComment = validateComment($commentValue);
        
        //overovani spravne validnosti hodnot jednotlivych inputu
		//spravne zapsane hodnoty vraci TRUE, spatne vyplnene FALSE

        $valid = $validTitle[1] && $validService[1] && $validComment[1];
        //funkce valid vrací true, pokud jsou všechna validace validní
        if ($valid) {
            include ('../DB/CreateConnection.php');
            include ('../DB/InsertComment.php');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Log IN</title>
  <link rel="stylesheet" media="screen" href="../static/css/style.css">
  <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
  <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="bookItPage">
  <header>
        <?php
            include("../include/Navigation.php");
        ?>
  </header>
  <main>
        <div class="BookIT">
            <span class="Book">Add </span><span class="getUnderline">Comment</span>
        </div>

        <div class="form">
            <form method="POST">
                <fieldset id="Name">
                    <label for="CustomerName"><legend>*Add your title</legend>
                    <input type="text" name="title" id="CustomerName" pattern="[A-Za-zěĚšŠčČřŘžŽýÝáÁíÍéÉÓó ]{4,}" value="" required title="Example: Milan" placeholder="name" >
                    </label>
                    <p id="errorName">
                    <?php
                        if(isset($validTitle)){
                            echo $validTitle[0];
                        }
                         //validtime[0] nám vrací chybovou hlášku a nebo nic
                        ?>
                    </p>
                </fieldset>

                <fieldset id="Email">
                    <label for="email"><legend>Choose a type of service</legend>
                    <select name="service" id="service">
                        <option value="<?php echo($info[0]) ?>"><?php echo($info[0]) ?></option>
                        <option value="<?php echo($info[1]) ?>"><?php echo($info[1]) ?></option>
                        <option value="<?php echo($info[2]) ?>"><?php echo($info[2]) ?></option>
                    </select>
                    </label>
                    <p id="errorName">
                    <?php
                        if(isset($validService)){
                            echo $validService[0];
                        }
                         //validtime[0] nám vrací chybovou hlášku a nebo nic
                        ?>
                    </p>
                </fieldset>
    
                <fieldset id="comment">
                    <label for="vzkaz"><legend>Add your comment here</legend>
                        <textarea id="vzkaz" cols="40" rows="8" name="text" placeholder="text..." maxlength="200" minlength="10"></textarea>
                    </label>
                    <p id="errorName">
                    <?php
                        if(isset($validComment)){
                            echo $validComment[0];
                        }
                         //validtime[0] nám vrací chybovou hlášku a nebo nic
                        ?>
                    </p>
                </fieldset>

                <fieldset>
                    <label for="image"><legend>Add your photo here</legend>
                        <input type="file" name="myImage" accept="image/x-png,image/gif,image/jpeg" />
                    </label>
                </fieldset>

                <fieldset id="buttons">
                    <input type="reset" value="RESET" />
                    <input type="submit" value="ADD" />
                </fieldset>
                <p class="required">* = required field</p>
            </form>
        </div>


    </main>
  <?php
    include("../include/Footer.php")
  ?>
</body>

</html>

