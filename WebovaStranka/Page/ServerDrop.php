<?php
    session_start();
    if (isset($_SESSION['server'])){
        if ($_SESSION['server'] == true){
            header ('Location: index.php');
            //kontroluju jestli existuje připojení, pokud ano, přesměruju se na hlavní stránku
        }
    } else {
        if (!isset($_SESSION['server'])){
            //pokud není vyžadováno spojení, stránka se přesměruje na domovksou
            header ('Location: index.php');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Not avaible right now</title>
    <link rel="stylesheet" media="screen" href="../static/css/style.css">
    <link rel="stylesheet" media="print" href="../static/cssPrint/stylePrint.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="../static/js/bookIt.js"></script>
</head>
<body class="serverDropPage">
    <header>
        <?php
            include("../include/Navigation.php");
        ?>
    </header>
    <div class="watermark"><img src="../static/img/LogoPrint.png" alt="watermark"></div>
    <div class="container">
        <p class="OOPS">OOOOOPS? &#9986;&#65039</p>
        <p class="error">SOMETHING HAPPENED, PLEASE TRY AGAIN LATER, SORRY...</p>
    </div>
    <?php
        include("../include/Footer.php")
    ?>
</body>
</html>