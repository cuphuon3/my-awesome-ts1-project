<?php
if(!isset($customerID)){
    //email, ktery neni v databazi
    $error = 'Wrong Combination';
} else {
    if (password_verify($PasswordValue, $customerPass)){
        //kontrola hesla
        $verify = true;
        $_SESSION['id'] = $customerID;
        //nastaví se sessionID, aby webová stránka věděla, že uživatel je přihlášený
        if ($customerEmail == "admin@zwabarber.shop"){
            //pokud byly zadané přihlašovací údaje administrátora, stránka se přesměruje na akkreservation.php
            header('Location: AllReservation.php');
        } else {
            $_SESSION['id'] = $customerID;
            header('Location: Booked.php');
        }
    } else {
        $verify = false;
        $_SESSION['id'] = null;
        $_SESSION['email'] = null;
        $error = 'Wrong Combination';
        //zadana kombinace byla špatně
    }    
}
?>