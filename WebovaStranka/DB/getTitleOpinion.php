<?php

$sql = "SELECT title FROM Titles";
$result = $connection->query($sql);
$info = array();

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    array_push($info, $row["title"]);  
  }
} else {
  echo "0 results";
}
mysqli_close($connection);
?>