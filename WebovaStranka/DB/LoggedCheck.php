<?php
    if (isset($_SESSION['id']) ? $_SESSION['id'] : null) {
        $logged = true;
        //na základe sessionID se kontroluje jestli je daný uživatel přihlašený nebo ne
    } else {
        $logged = false;
        //pokud uživatel neni přihlaseny, stránka se přesměruje na login.php.
        header("Location: Login.php");
    }
?>