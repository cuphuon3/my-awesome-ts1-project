<nav class="navigation">
    <div>
        <img src="../static/img/Logo2.png" alt="logo" class="logo">
    </div>

    <div>
        <a href="index.php">Home</a>
        <a href="Gallery.php">Gallery</a>
        <a href="PriceList.php">Price List</a>
        <a href="ChooseDay.php">Make a Reservation</a>
        <a href="AboutUs.php">About Us</a>
        <a href="ContactUs.php">Contact Us</a>
        <?php
            if(isset($_SESSION['id'])){
                //pokud je prihlaseny nejaky uzivatel
                include("../DB/CreateConnection.php");
                include("../DB/GetCustomer.php");
                if ($customerEmail == 'admin@zwabarber.shop'){
                    echo('<a href="AllReservation.php">All Reservation</a>');
                    // pokud je přihlášený uživatel personál barber shopu
                } else {
                    echo('<a href="Booked.php">My Reservation</a>');
                    echo('<a href="AddComment.php">Add Comment</a>');
                    // pokud je přihlášený uživatel normální zákazník
                } 
            } else {
                echo('<a href="Booked.php">Log In</a>');
            } 
        ?>
        <a href="AllComments.php">All Comment</a>
    </div>
</nav>
