<?php
    //Soubor z funkcemi pro validace

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    //startuju session

    function validateEmail($emailValue){
        if (strlen($emailValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR EMAIL';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (substr_count($emailValue, "@") > 0){
            $_SESSION['email'] = $emailValue;
            $error = '';
            return [$error, true];
        } else {
            $error = 'INVALID FORMAT';
            return [$error, false];
            //pokud bude chybet @, vracím chybovou hlášku a false
        }
    }

    function validateName($nameValue){
        if (strlen($nameValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR NAME';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (strlen($nameValue) > 3){
            //pokud je jemno alespon 4 znaky dlouhý, validuju dál
            if (preg_match("/^[a-zA-Z-' ]*$/",$nameValue)) {
                $_SESSION['name'] = $nameValue;
                $error = '';
                return [$error, true];
                //jemno nesmi obsahovat jine znaky než znaky abecedy
            } else {
                $error = 'INVALID NAME, PLEASE TYPE AGAIN';
                return [$error, false];
            }
        }  else {
            $error = 'THE NAME IS TO SHORT';
            return [$error, false];
        }
    }

    function validateTitle($titleValue){
        if (strlen($titleValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE TITLE';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (strlen($titleValue) > 3){
            //pokud je jemno alespon 4 znaky dlouhý, validuju dál
            if (preg_match("/^[a-zA-Z-' ]*$/",$titleValue)) {
                $_SESSION['name'] = $titleValue;
                $error = '';
                return [$error, true];
                //jemno nesmi obsahovat jine znaky než znaky abecedy
            } else {
                $error = 'INVALID TITLE, PLEASE TYPE AGAIN';
                return [$error, false];
            }
        }  else {
            $error = 'THE TITLE IS TO SHORT';
            return [$error, false];
        }
    }

    function validateComment($commentValue){
        if (strlen($commentValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE COMMENT';
            return [$error, false];
            //vracím chybovou hlášku a false
        } else {
            $error = '';
            return [$error, true];
        }
    }

    function validateCovid($covidValue){
        if($covidValue == "ano"){
            $error = '';
            return [$error, true];
        } else {
            $error = 'Pokud jste nevybral ANO, nemuzete vytvorit rezervaci';
            return [$error, false];
        }
    }

    function validateService($serviceValue){
        if (strlen($serviceValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE CHOOSE TITLE';
            return [$error, false];
            //vracím chybovou hlášku a false
        }
        if ($serviceValue == "Hair cutting" || $serviceValue == "Hair coloring" || $serviceValue == "Hair styling"){
            $error = '';
            return [$error, true];
        } else {
            $error = 'Please Choose Again';
            return [$error, false];
        }
    }

    function validatePassword($unHashPass){
        if (strlen($unHashPass) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR PASSWORD';
            return [$error, false];
        }
        if (strlen($unHashPass) > 5){
            //heslo musí být alespon 6znaků dlouhý
            $error = '';
            return [$error, true];
        } else {
            $error = 'THE PASSWORD IS TO SHORT';
            return [$error, false];
        }
    }

    function validatePasswordCheck($passwordCheck, $unHashPass){
        if (validatePassword($unHashPass)[1] == true){
            //funkce validuje jenom v případě, že je validní první heslo
            if (strlen($unHashPass) == 0){
                //pokud je input prázdný
                $error = 'PLEASE TYPE YOUR PASSWORD';
                return [$error, false];
            }
            if ($passwordCheck != $unHashPass){
                $error = 'THE PASSWORDS DID NOT MATCH';
                return [$error, false];
            } else {
                $error = '';
                return [$error, true];
            }
        } else {
            $error = '';
            return [$error, true];
        }
    }

    function validateTime($timeValue){
        if ($timeValue == ''){
            //pokud je input prázdný
            $error = 'PLEASE CHOOSE A TIME';
            return [$error, false];
        }
        include("../DB/CreateConnection.php");
        include("../DB/GetTime.php");
        //vytvářím spojení se serverem a nazákladě data v session[day], vracím pole, kde jsou jen rezervovane casy v daném dni
        //freeTime = pole s již rezervovaným časem
        if (in_array($timeValue, $freeTime)){
            $error = 'THIS TIME HAS BEEN ALREADY RESERVED BY ANOTHER CUSTOMER';
            //pokud je vybraný čas v poli, vracím chybu
            return [$error, false];
        }
        $time = array('9:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');
        if (in_array($timeValue, $time)){
            $error = '';
            //pokud vybraný čas je v poli time, vracím ture
            return [$error, true];
        } else {
            $error = 'INVALID TIME, PLEASE CHOOSE AGAIN';
            return [$error, false];
        }
    }

    function validateTelNumber($telnumberValue){
        if (strlen($telnumberValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR TELEPHONE NUMBER';
            return [$error, false];
        }

        if(preg_match("/^[0-9]{3}[0-9]{3}[0-9]{3}$/", $telnumberValue)) {
            $_SESSION['TelNum'] = $telnumberValue;
            $error = '';
            //mohou obsahovat pouze čísla
            return [$error, true];
        } else {
            $error = 'INVALID FORMAT';
            return [$error, false];
        }
    }

    function validateDay($dayValue){
        $day1 = date('Y-m-d', strtotime(' +1 day'));
        $day2 = date('Y-m-d', strtotime(' +2 day'));
        $day3 = date('Y-m-d', strtotime(' +3 day'));
        $day4 = date('Y-m-d', strtotime(' +4 day'));
        $day5 = date('Y-m-d', strtotime(' +5 day'));
        $day6 = date('Y-m-d', strtotime(' +6 day'));
        $day7 = date('Y-m-d', strtotime(' +7 day'));
        $days = array($day1,$day2,$day3,$day4,$day5,$day6,$day7);
        //days je pole dnu v rozmezí den+1 až den+8

        if (in_array($dayValue, $days)){
            return true;
            //pokud se vybraný den vyskytuje v poli, vracím true
        } else {
            return false;
        }
    }
?>
