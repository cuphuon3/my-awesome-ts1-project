function validateEmail(event){  
    var email = document.querySelector('#email');
    var str = email.value.length;

    if (str == 0){
        event.preventDefault();
        document.getElementById("errorEmail").innerHTML = 'PLEASE TYPE YOUR EMAIL';
        return false;
    } else {
        if (email.value.indexOf('@') == -1){
            event.preventDefault();
            document.getElementById("errorEmail").innerHTML = 'INVALID FORMAT';
            return false;
        } else {
            document.getElementById("errorEmail").innerHTML = '';
            return true;
        }
    }
}

function validatePassword(event){  
    var password = document.querySelector('#password');
    var str = password.value.length;

    if (str == 0){
        event.preventDefault();
        document.getElementById("errorPass").innerHTML = 'PLEASE TYPE YOUR PASSWORD';
        return false;
    } else {
        if (str < 6){
            
            document.getElementById("errorPass").innerHTML = 'THE PASSWORD IS TO SHORT';
            return false;
        } else {
            document.getElementById("errorPass").innerHTML = '';
            return true;
        }
    }
}

function validatePasswordCheck(event){  
    timerID = null;
    var password = document.querySelector('#password');
    var passwordCheck = document.querySelector('#passwordCheck');

    if (validatePassword(event)){
        if (password.value != passwordCheck.value){
            document.getElementById('errorPassCheck').innerHTML = 'THE PASSWORDS DO NOT MATCH';
            event.preventDefault();
            return false
        }  else {
            document.getElementById('errorPassCheck').innerHTML = '';
            return true
        }
    } else {
        document.getElementById('errorPassCheck').innerHTML = '';
    }
}


function validateName(event){
    var letters = /^[A-Za-z]+$/;
    var CustomerName = document.querySelector('#CustomerName');
    var str = CustomerName.value.length;

    if(str == 0){
        event.preventDefault();
        document.getElementById("errorName").innerHTML = 'PLEASE TYPE YOUR NAME';
        return false;
    } else {
        if (str <4){
            event.preventDefault();
            document.getElementById("errorName").innerHTML = 'THE NAME IS TO SHORT';
            return false;
        } else {
            if(CustomerName.value.match(letters)){
                document.getElementById("errorName").innerHTML = '';
                return true;
            } else {
                document.getElementById("errorName").innerHTML = 'INVALID NAME';
                event.preventDefault();
                return false;
            }
        }
    }
}

function validateTelNum(event){
  var phoneno = /^\+?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
  var telnumber = document.querySelector('#telnumber');
  var int = telnumber.value.length;

  if(int == 0){
    event.preventDefault();
    document.getElementById("errorTel").innerHTML = 'PLEASE TYPE YOUR TELEPHONE NUMBER';
    return false;
}

  if((telnumber.value.match(phoneno))){
    document.getElementById("errorTel").innerHTML = '';
    return true;
    } else {
        event.preventDefault();
        document.getElementById("errorTel").innerHTML = 'INVALID TELEPHONE NUMBER, PLEASE TYPE AGAIN';
        return false;
    }
}

function validateTime(event){
    var time = document.getElementsByName('time');
    var formValid = false;

    var i = 0;
    while (!formValid && i < time.length) {
        if (time[i].checked) formValid = true;
        i++;        
    }

    var i = 0;

    if (!formValid) {
        document.getElementById("errorTime").innerHTML = 'PLEASE CHOOSE A TIME';
        event.preventDefault();
        return false;
    } else {
        var formValid = false;
        while (!formValid) {
            if (time[i].checked) formValid = true;
            var c = i;
            i++;        
        } 
        // console.log(time[c].value)
        if (getTime(1).includes(time[c])){
            document.getElementById("errorTime").innerHTML = 'THIS TIME HAS BEEN ALREADY RESERVED BY ANOTHER CUSTOMER';
            event.preventDefault();
            return false;
        } else {
            if (getTime(2).includes(time[c].value)){
                document.getElementById("errorTime").innerHTML = '';
                return true;
            } else {
                document.getElementById("errorTime").innerHTML = 'PLEASE CHOOSE AGAIN';
                event.preventDefault();
                return false;
            }
        }
    }
}

function getTime(i){
    if (i == 0) {
        var time = document.getElementsByName('time');
        var ResTime = [];
        var freetime = []
        var i = 0;
        while (i != 8) {
            if (time[i].disabled == true){
                ResTime.push(time[i]);
            }
            if (time[i].disabled == ''){
                freetime.push(time[i].value);
            }
            i++;
        }
        x = ResTime;
        z = freetime;
    }
    if (i == 1) {
        return x;
    }
    if (i == 2){
        return z;
    }
}

function detectEnd(event){
    if (timerID != null){
        clearInterval(timerID);
        timerID = null;
    }
    timerID = setTimeout(function(){validatePasswordCheck(event)}, 500);
}

function init(){
    var formular = document.querySelector('div>form');
    email.addEventListener('blur', validateEmail);
    password.addEventListener('blur', validatePassword);
    passwordCheck.addEventListener('keyup', detectEnd);
    telnumber.addEventListener('blur', validateTelNum);
    CustomerName.addEventListener('blur', validateName);
    formular.addEventListener('submit', validateEmail);
    formular.addEventListener('submit', validatePassword);
    formular.addEventListener('submit', validatePasswordCheck);
    formular.addEventListener('submit', validateTelNum);
    formular.addEventListener('submit', validateName);
    formular.addEventListener('submit', validateTime);
    window.addEventListener('load', getTime(0));
}
