function GetDay(){
    var c = 1
    var days = [];
    while (c != 8){
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + c);
        var day = currentDate;
        var month = (day.getMonth()+1);
        if (month.toString().length == 1){
            month = '0'+(day.getMonth()+1);
        }
        var dayx = (day.getDate());
        if (dayx.toString().length == 1){
            dayx = '0'+(day.getDate());
        }
        var day_ = ((day.getFullYear())+'-'+(month)+'-'+(dayx));
        days.push(day_);
        c++;
    }
    return days;
}

function validateDate(event){
    var days = GetDay();
    var date = document.querySelector('input[type=date]');
    var n = days.includes(date.value);

    if (n == false){
        console.log(date.value)
        event.preventDefault();
        date.classList.add('error');
        date.classList.add('shake');
        document.getElementById("error").innerHTML = 'PLEASE CHOOSE DAY IN INTERVAL ' + days[0] + ' TO ' + days[6];
        console.log(days);
    } else {
        date.classList.remove('error');
    }
}

function init(){
    var formular = document.querySelector('div>form');
    choosedDay.addEventListener('blur',validateDate);
    formular.addEventListener('submit', validateDate);
}


