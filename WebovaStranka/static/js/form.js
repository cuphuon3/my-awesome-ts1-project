function validate(event){  
    var email = document.querySelector('#email');
    // console.log(email);

    if (email.value.indexOf('@') == -1){
        event.preventDefault();
        email.classList.add('error');
        email.classList.add('shake');
    } else {
        email.classList.remove('error');
    }
}

function validatePassword(event){  
    var password = document.querySelector('#password');
    var str = password.value.length;
    // console.log(password);

    if (str < 6){
        event.preventDefault();
        password.classList.add('error');
        password.classList.add('shake');
    } else {
        password.classList.remove('error');
    }
}

function init(){
    var formular = document.querySelector('.Booking>form');
    email.addEventListener('blur', validate);
    password.addEventListener('blur', validatePassword);
    formular.addEventListener('submit', validate);
    formular.addEventListener('submit', validatePassword);
}


