<?php

class ValidateEmail{

    public function __construct()
    {

    }

    function validateEmail($emailValue){
        if (strlen($emailValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR EMAIL';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (substr_count($emailValue, "@") > 0){
            $_SESSION['email'] = $emailValue;
            $error = '';
            return [$error, true];
        } else {
            $error = 'INVALID FORMAT';
            return [$error, false];
        }
    }
}

