<?php


class validateTitle
{

    /**
     * validateTitle constructor.
     */
    public function __construct()
    {
    }

    function validateTitle($titleValue){
        if (strlen($titleValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE TITLE';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (strlen($titleValue) > 3){
            //pokud je jemno alespon 4 znaky dlouhý, validuju dál
            if (preg_match("/^[a-zA-Z-' ]*$/",$titleValue)) {
                $_SESSION['name'] = $titleValue;
                $error = '';
                return [$error, true];
                //jemno nesmi obsahovat jine znaky než znaky abecedy
            } else {
                $error = 'INVALID TITLE, PLEASE TYPE AGAIN';
                return [$error, false];
            }
        }  else {
            $error = 'THE TITLE IS TO SHORT';
            return [$error, false];
        }
    }
}