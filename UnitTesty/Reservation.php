<?php
class Reservation{


    /**
     * DeleteReservation constructor.
     */
    public function __construct()
    {
    }

    public function deleteCustomer($id, $customerList){
        $i = 0;
        foreach ($customerList as $value) {
            if($value['id'] == $id){
                unset($customerList[$i]);
            }
            $i++;
        }
        return $customerList;
    }

    public function getCutomerById($id, $customerList){
        foreach ($customerList as $value) {
            if($value['id'] == $id){
                return($value);
            }
        }
    }

    public function checkDuplicityEmail($email, $customerList){
        foreach ($customerList as $value) {
            if($value['email'] == $email){
                $error = "You already have a reservation";
                return [$error, false];
            }
        }
        $error = "";
        return [$error, true];
    }

    public function validationChoosedTime($timeValue,$freeTime){
        if ($timeValue == ''){
            //pokud je input prázdný
            $error = 'PLEASE CHOOSE A TIME';
            return [$error, false];
        }
        //vytvářím spojení se serverem a nazákladě data v session[day], vracím pole, kde jsou jen rezervovane casy v daném dni
        //freeTime = pole s již rezervovaným časem
        if (in_array($timeValue, $freeTime)){
            $error = 'THIS TIME HAS BEEN ALREADY RESERVED BY ANOTHER CUSTOMER';
            //pokud je vybraný čas v poli, vracím chybu
            return [$error, false];
        }
        $time = array('9:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');
        if (in_array($timeValue, $time)){
            $error = '';
            //pokud vybraný čas je v poli time, vracím ture
            return [$error, true];
        } else {
            $error = 'INVALID TIME, PLEASE CHOOSE AGAIN';
            return [$error, false];
        }
    }
}
