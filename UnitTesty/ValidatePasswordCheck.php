<?php
include ("ValidatePassword.php");

class ValidatePasswordCheck
{

    /**
     * ValidatePasswordCheck constructor.
     */
    public function __construct()
    {
    }



    function validatePasswordCheck($passwordCheck, $unHashPass){
        $validatePasswordClass = new ValidatePassword();
        if ($validatePasswordClass->validatePassword($unHashPass)[1] == true){
            //funkce validuje jenom v případě, že je validní první heslo
            if (strlen($unHashPass) == 0){
                //pokud je input prázdný
                $error = 'PLEASE TYPE YOUR PASSWORD';
                return [$error, false];
            }
            if ($passwordCheck != $unHashPass){
                $error = 'THE PASSWORDS DID NOT MATCH';
                return [$error, false];
            } else {
                $error = '';
                return [$error, true];
            }
        } else {
            $error = '';
            return [$error, true];
        }
    }
}