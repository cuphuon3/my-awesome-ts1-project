<?php


class ValidateTelNumber
{

    /**
     * ValidateTelNumber constructor.
     */
    public function __construct()
    {
    }

    function validateTelNumber($telnumberValue){
        if (strlen($telnumberValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR TELEPHONE NUMBER';
            return [$error, false];
        }

        if(preg_match("/^[0-9]{3}[0-9]{3}[0-9]{3}$/", $telnumberValue)) {
            $_SESSION['TelNum'] = $telnumberValue;
            $error = '';
            //mohou obsahovat pouze čísla
            return [$error, true];
        } else {
            $error = 'INVALID FORMAT';
            return [$error, false];
        }
    }
}