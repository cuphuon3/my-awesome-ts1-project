<?php


class ValidateDay
{

    /**
     * ValidateDay constructor.
     */
    public function __construct()
    {
    }

    function validateDay($dayValue){
        $day1 = date('Y-m-d', strtotime(' +1 day'));
        $day2 = date('Y-m-d', strtotime(' +2 day'));
        $day3 = date('Y-m-d', strtotime(' +3 day'));
        $day4 = date('Y-m-d', strtotime(' +4 day'));
        $day5 = date('Y-m-d', strtotime(' +5 day'));
        $day6 = date('Y-m-d', strtotime(' +6 day'));
        $day7 = date('Y-m-d', strtotime(' +7 day'));
        $days = array($day1,$day2,$day3,$day4,$day5,$day6,$day7);
        //days je pole dnu v rozmezí den+1 až den+8

        if (in_array($dayValue, $days)){
            return true;
            //pokud se vybraný den vyskytuje v poli, vracím true
        } else {
            return false;
        }
    }
}