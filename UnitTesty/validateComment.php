<?php


class validateComment
{

    /**
     * validateComment constructor.
     */
    public function __construct()
    {
    }

    function validateComment($commentValue){
        if (strlen($commentValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE COMMENT';
            return [$error, false];
            //vracím chybovou hlášku a false
        } else {
            $error = '';
            return [$error, true];
        }
    }
}