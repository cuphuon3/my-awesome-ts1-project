<?php
class Comment{

    /**
     * Comment constructor.
     */
    public function __construct()
    {
    }

    function validateService($choosedService, $serviceValue){
        if (strlen($choosedService) == 0){
            //pokud je input prázdný
            $error = 'PLEASE CHOOSE TITLE';
            return [$error, false];
            //vracím chybovou hlášku a false
        }
        if (in_array($choosedService, $serviceValue)){
            $error = '';
            return [$error, true];
        } else {
            $error = 'Please Choose Again';
            return [$error, false];
        }
    }
}