<?php

class ValidateTelNumberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $telNum;
    
    protected function _before()
    {
        $this->telNum = new ValidateTelNumber();
    }

    protected function _after()
    {
    }

    // tests
    public function testEmptyInputReturnsError()
    {
        $this->assertEquals('PLEASE TYPE YOUR TELEPHONE NUMBER',$this->telNum->ValidateTelNumber("")[0]);
    }

    public function testIncorrectInputReturnsError()
    {
        $this->assertEquals('INVALID FORMAT',$this->telNum->ValidateTelNumber("brno123")[0]);
    }

    public function testCorrectInputReturnsTrue()
    {
        $this->assertTrue($this->telNum->ValidateTelNumber("123456789")[1]);
    }
}