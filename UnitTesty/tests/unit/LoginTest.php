<?php

class LoginTest extends \Codeception\Test\Unit
{
    protected $tester;
    protected $login;
    
    protected function _before()
    {
        $this->login = new Login();
    }

    protected function _after()
    {
    }

    // tests
    public function testSuccefullyLogin()
    {
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $Customer = ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'];

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $passwordInput = 'Milan1';
        $emailInput = 'phuong.dong.cu@gmail.com';

        $this->assertTrue($this->login->login($passwordInput, $emailInput, $customerList));
    }

    public function testFailedLogin()
    {
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $Customer = ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'];

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $passwordInput = 'Milan2';
        $emailInput = 'phuong.dong.cu@gmail.com';

        $this->assertFalse($this->login->login($passwordInput, $emailInput, $customerList));
    }
}