<?php

class ValidateNameTest extends \Codeception\Test\Unit
{

    protected $tester;
    protected $name;
    
    protected function _before()
    {
        $this->name = new ValidateName();
    }

    protected function _after()
    {
    }

    // tests
    public function testInsertShortInputReturnsErrorMessage()
    {
        $this->assertEquals('THE NAME IS TOO SHORT',$this->name->validateName("lk")[0]);
    }

    public function testWrongInsertReturnsFalse()
    {
        $this->assertFalse($this->name->validateName("lk")[1]);
    }

    public function testInvalidInputReturnsErrorMessage()
    {
        $this->assertEquals('INVALID NAME, PLEASE TYPE AGAIN',$this->name->validateName("5ehořurim <|€")[0]);
    }

    public function testNoInputReturnsErrorMessage()
    {
        $this->assertEquals('PLEASE TYPE YOUR NAME',$this->name->validateName("")[0]);
    }

    public function testCorrectInputReturnsEmptyErrorMessage()
    {
        $this->assertEquals('',$this->name->validateName("Prokop Buben")[0]);
    }

    public function testCorrectInputReturnsTrue()
    {
        $this->assertTrue($this->name->validateName("Prokop Buben")[1]);
    }
}