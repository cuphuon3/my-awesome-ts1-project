<?php

class ValidateCovidTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $covid;
    
    protected function _before()
    {
        $this->covid = new ValidateCovid();
    }

    protected function _after()
    {
    }

    // tests
    public function testWrongSelectionReturnsError()
    {
        $this->assertEquals('Pokud jste nevybral ANO, nemuzete vytvorit rezervaci',$this->covid->ValidateCovid("nevím")[0]);
    }

    public function testWrongSelectionReturnsFalse()
    {
        $this->assertFalse($this->covid->ValidateCovid("nevím")[1]);
    }

    public function testCorrectSelectionReturnsTrue()
    {
        $this->assertTrue($this->covid->ValidateCovid("ano")[1]);
    }
}