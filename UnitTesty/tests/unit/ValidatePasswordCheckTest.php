<?php

class ValidatePasswordCheckTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $password;
    
    protected function _before()
    {
        $this->password = new ValidatePasswordCheck();
    }

    protected function _after()
    {
    }

    // tests
    public function testUnmatchingPasswordsReturnError()
    {
        $this->assertEquals('THE PASSWORDS DID NOT MATCH',$this->password->ValidatePasswordCheck("passwor","password")[0]);
    }

    public function testMatchingPasswordsReturnTrue()
    {
        $this->assertTrue($this->password->ValidatePasswordCheck("password","password")[1]);
    }
}