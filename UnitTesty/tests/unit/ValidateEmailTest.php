<?php

class ValidateEmailTest extends \Codeception\Test\Unit
{
    protected $tester;
    protected $email;
    
    protected function _before()
    {
        $this->email = new ValidateEmail();
    }

    protected function _after()
    {
    }

    // tests
    public function testEmailValidationReturnTrue()
    {
        $this->assertTrue($this->email->validateEmail("cu@seznam.cz")[1]);
    }

    public function testEmailValidationReturnNoErrorMessage()
    {
        $this->assertEquals("",$this->email->validateEmail("cu@seznam.cz")[0]);
    }

    public function testEmailValidationReturnFalse()
    {
        $this->assertFalse($this->email->validateEmail("cuseznam.cz")[1]);
    }

    public function testEmailValidationReturnErrorMessage()
    {
        $this->assertEquals("INVALID FORMAT",$this->email->validateEmail("cuseznam.cz")[0]);
    }

    public function testEmailValidationEntryNullRetrunErrorMessage()
    {
        $this->assertEquals("PLEASE TYPE YOUR EMAIL",$this->email->validateEmail("")[0]);
    }
}