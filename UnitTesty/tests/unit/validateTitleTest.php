<?php

class validateTitleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $title;
    
    protected function _before()
    {
        $this->title = new ValidateTitle();
    }

    protected function _after()
    {
    }

    // tests
    public function testNoInputReturnsErrorMessage()
    {
        $this->assertEquals('PLEASE TYPE TITLE',$this->title->validateTitle("")[0]);
    }

    public function testInvalidInputReturnsErrorMessage()
    {
        $this->assertEquals('INVALID TITLE, PLEASE TYPE AGAIN',$this->title->validateTitle("|€<ß¤÷")[0]);
    }

    public function testShortInputReturnsErrorMessage()
    {
        $this->assertEquals('THE TITLE IS TO SHORT',$this->title->validateTitle("br")[0]);
    }

    public function testCorrectInputReturnsTrue()
    {
        $this->assertTrue($this->title->validateTitle("Titulek")[1]);
    }
}