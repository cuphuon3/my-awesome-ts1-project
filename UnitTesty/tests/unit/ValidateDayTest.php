<?php

class ValidateDayTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $day;
    
    protected function _before()
    {
        $this->day = new ValidateDay();
    }

    protected function _after()
    {
    }

    // tests
    public function testPastDayInputReturnsFalse()
    {
        $this->assertFalse($this->day->ValidateDay(2001-03-10));
    }

    public function testFutureDayInputReturnsTrue()
    {
        $this->assertFalse($this->day->ValidateDay(2021-05-22));
    }
}