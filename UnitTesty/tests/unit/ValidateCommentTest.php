<?php

class ValidateCommentTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $comment;
    
    protected function _before()
    {
        $this->comment = new validateComment();
    }

    protected function _after()
    {
    }

    // tests
    public function testEmptyCommentReturnsError()
    {
        $this->assertEquals('PLEASE TYPE COMMENT',$this->comment->validateComment("")[0]);
    }

    public function testNonEmptyCommentReturnsEmptyError()
    {
        $this->assertEquals('',$this->comment->validateComment("Gda")[0]);
    }

    public function testNonEmptyCommentReturnsTrue()
    {
        $this->assertTrue($this->comment->validateComment("Gda")[1]);
    }
}