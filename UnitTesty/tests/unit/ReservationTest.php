<?php

class ReservationTest extends \Codeception\Test\Unit
{
    protected $tester;
    protected $reservation;


    protected function _before()
    {
        $this->reservation = new Reservation();
    }

    protected function _after()
    {
    }

    public function testDeleteReservationTest(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $customerToDelete = ['id' => '5fe8da4a363fb', 'name' => 'Milan'];

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertFalse(in_array($customerToDelete, $this->reservation->deleteCustomer($customerToDelete['id'], $customerList)));
    }

    public function testGetReservationByCustomerId(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $expectedCustomer = ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'];

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertEquals($expectedCustomer, $this->reservation->getCutomerById($expectedCustomer['id'], $customerList));
    }


    public function testEmailIsAlreadyInSystemReturnFalse(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $typedEmail = "phuong.dong.cu@gmail.com";

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertFalse($this->reservation->checkDuplicityEmail($typedEmail, $customerList)[1]);
    }

    public function testEmailIsAlreadyInSystemReturnErrorMessage(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $typedEmail = "phuong.dong.cu@gmail.com";

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertEquals( "You already have a reservation",$this->reservation->checkDuplicityEmail($typedEmail, $customerList)[0]);
    }

    public function testEmailIsNotInSystemReturnErrorMessage(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $typedEmail = "phuong.dong@gmail.com";

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertEquals( "",$this->reservation->checkDuplicityEmail($typedEmail, $customerList)[0]);
    }

    public function testEmailIsNotInSystemReturnTrue(){
        $customerList = [
            ['id' => '5fe8da4a363fb', 'name' => 'Milan', 'email' => 'phuong.dong.cu@gmail.com', 'password' => 'Milan1'],
            ['id' => '5fe8dbbf86b21', 'name' => 'admin', 'email' => 'admin@zwabarber.shop', 'password' => 'Admin1'],
            ['id' => '5fe8e1be01c82', 'name' => 'Jakub', 'email' => 'jakub@cvut.cz', 'password' => 'Jakub1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e22e1e92a', 'name' => 'honza', 'email' => 'honza@cvut.cz', 'password' => 'Honza1'],
            ['id' => '5fe8e27c8ba72', 'name' => 'tomas', 'email' => 'tomas@cvut.cz', 'password' => 'Tomas1']
        ];

        $typedEmail = "phuong.dong@gmail.com";

        $db_mocked = Mockery::mock('CustomerDB');
        $db_mocked
            ->shouldReceive('getCustomer')
            ->once()
            ->andReturn($customerList);

        $this->assertTrue( $this->reservation->checkDuplicityEmail($typedEmail, $customerList)[1]);
    }

    public function testChoosedTimeIsNotAvailableReturnFalse(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '11:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);



        $this->assertFalse( $this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[1]);
    }

    public function testChoosedTimeIsNotAvailableReturnErrorMessage(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '11:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertEquals("THIS TIME HAS BEEN ALREADY RESERVED BY ANOTHER CUSTOMER", $this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[0]);
    }

    public function testChoosedInvalidTimeReturnFalse(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '90:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertFalse($this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[1]);
    }

    public function testChoosedInvalidTimeReturnErrorMessage(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '115:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertEquals("INVALID TIME, PLEASE CHOOSE AGAIN",$this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[0]);
    }

    public function testChoosedValidTimeReturnNoErrorMessage(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '9:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertEquals("",$this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[0]);
    }

    public function testChoosedValidTimeReturnTrue(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '9:00:00';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertTrue($this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[1]);
    }

    public function testSendEmptyFieldReturnFalse(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertFalse($this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[1]);
    }

    public function testSendEmptyFieldReturnErrorMessage(){
        $availableTimeList = array('11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00');

        $choosedTime = '';

        $db_mocked = Mockery::mock('ReservationDB');
        $db_mocked
            ->shouldReceive('getTime')
            ->once()
            ->andReturn($availableTimeList);

        $this->assertEquals("PLEASE CHOOSE A TIME", $this->reservation->validationChoosedTime($choosedTime, $availableTimeList)[0]);
    }
}