<?php

class ValidatePasswordTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $password;
    
    protected function _before()
    {
        $this->password = new ValidatePassword();
    }

    protected function _after()
    {
    }

    // tests
    public function testEmptyInputReturnsError()
    {
        $this->assertEquals('PLEASE TYPE YOUR PASSWORD',$this->password->ValidatePassword("")[0]);
    }

    public function testShortInputReturnsError()
    {
        $this->assertEquals('THE PASSWORD IS TO SHORT',$this->password->ValidatePassword("pass")[0]);
    }

    public function testCorrectPasswordReturnsTrue()
    {
        $this->assertTrue($this->password->ValidatePassword("password")[1]);
    }
}