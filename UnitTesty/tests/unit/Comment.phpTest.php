<?php

class CommentTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $comment;
    
    protected function _before()
    {
        $this->comment = new Comment();
    }

    protected function _after()
    {
    }

    // tests
    public function testValidateChoosedValidServiceReturnTrue()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = 'Hair cutting';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertTrue($this->comment->validateService($choosedService, $services)[1]);
    }

    public function testValidateChoosedValidServiceReturnNoErrorMessage()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = 'Hair cutting';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertEquals("",$this->comment->validateService($choosedService, $services)[0]);
    }

    public function testValidateChoosedInvalidServiceReturnErrorMessage()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = 'auto';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertEquals("Please Choose Again",$this->comment->validateService($choosedService, $services)[0]);
    }

    public function testValidateChoosedInvalidServiceReturnFalse()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = 'auto';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertFalse($this->comment->validateService($choosedService, $services)[1]);
    }

    public function testValidateEmptyFieldReturnFalse()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = '';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertFalse($this->comment->validateService($choosedService, $services)[1]);
    }

    public function testValidateEmptyFieldReturnErrorMessage()
    {
        $services = array("Hair cutting" ,"Hair coloring", "Hair styling");

        $choosedService = '';

        $db_mocked = Mockery::mock('CommentDB');
        $db_mocked
            ->shouldReceive('getService')
            ->once()
            ->andReturn($services);

        $this->assertEquals("PLEASE CHOOSE TITLE",$this->comment->validateService($choosedService, $services)[0]);
    }
}