<?php


class ValidatePassword
{

    /**
     * ValidatePassword constructor.
     */
    public function __construct()
    {
    }

    function validatePassword($unHashPass){
        if (strlen($unHashPass) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR PASSWORD';
            return [$error, false];
        }
        if (strlen($unHashPass) > 5){
            //heslo musí být alespon 6znaků dlouhé
            $error = '';
            return [$error, true];
        } else {
            $error = 'THE PASSWORD IS TO SHORT';
            return [$error, false];
        }
    }
}