<?php
class ValidateName{
    /**
     * ValidateName constructor.
     */
    public function __construct()
    {
    }


    function validateName($nameValue){
        if (strlen($nameValue) == 0){
            //pokud je input prázdný
            $error = 'PLEASE TYPE YOUR NAME';
            return [$error, false];
            //vracím chybovou hlášku a false
        }

        if (strlen($nameValue) > 3){
            //pokud je jemno alespon 4 znaky dlouhý, validuju dál
            if (preg_match("/^[a-zA-Z-' ]*$/",$nameValue)) {
                $_SESSION['name'] = $nameValue;
                $error = '';
                return [$error, true];
                //jemno nesmi obsahovat jine znaky než znaky abecedy
            } else {
                $error = 'INVALID NAME, PLEASE TYPE AGAIN';
                return [$error, false];
            }
        }  else {
            $error = 'THE NAME IS TOO SHORT';
            return [$error, false];
        }
    }


}
