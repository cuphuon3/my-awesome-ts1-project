package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestLinks {

    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;
    private Footer footer;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Order(1)
    @Test
    public void TestNavigationBar(){
        navigationBar = new NavigationBar(driver);

        gallery = navigationBar.goToGalleryPage();
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/Gallery.php", gallery.getDriver().getCurrentUrl());

        priceList = navigationBar.goToPriceListPage();
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/PriceList.php", priceList.getDriver().getCurrentUrl());

        aboutUs = navigationBar.goToAboutUsPage();
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/AboutUs.php", aboutUs.getDriver().getCurrentUrl());

        contactUs = navigationBar.goToContactUsPage();
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/ContactUs.php", contactUs.getDriver().getCurrentUrl());

        allComment = navigationBar.goToAllCommentPage();
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/AllComments.php", allComment.getDriver().getCurrentUrl());

        assertTrue((navigationBar.getLoginButton().isDisplayed()));
    }

    @Order(2)
    @Test
    public void testFooterLinks() throws InterruptedException {
        footer = new Footer(driver);

        String email = footer.getGoogle().getAttribute("href");
        assertEquals("mailto:phuong.dong.cu@gmail.com?Subject=Reservation", email);

        footer.clickOnIg();
        ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs_windows.get(1));
        Thread.sleep(3000);
        assertEquals("https://www.instagram.com/milancu__design/?hl=cs", driver.getCurrentUrl());
    }

    @AfterAll
    public static void cleanUp() {
        driver.quit();
    }
}
