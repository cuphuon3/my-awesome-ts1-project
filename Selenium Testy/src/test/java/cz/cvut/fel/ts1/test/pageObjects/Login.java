package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {
    WebDriver driver;

    @FindBy(className = "Booking")
    WebElement form;

    @FindBy(id = "email")
    WebElement emailField;

    @FindBy(id = "password")
    WebElement passwordField;

    @FindBy(id = "submit")
    WebElement submit;

    @FindBy(id = "registration")
    WebElement registration;

    public static Login goTo(String baseUrl, WebDriver driver) {
        Login login =  new Login(driver);
        driver.get(baseUrl);
        return login;
    }

    public Login(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getForm() {
        return form;
    }

    public WebElement getEmailField() {
        return emailField;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getRegistration() {
        return registration;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void fillEmail(String emailToFill){
        if(!(emailField.getText().equals(""))){
            emailField.clear();
        }
        emailField.sendKeys(emailToFill);
    }

    public void fillPassword(String passwordToFill){
        passwordField.clear();
        passwordField.sendKeys(passwordToFill);
    }

    public Booked submit(){
        submit.click();
        return new Booked(driver);
    }

    public ChooseDay registration(){
        registration.click();
        return new ChooseDay(driver);
    }
}
