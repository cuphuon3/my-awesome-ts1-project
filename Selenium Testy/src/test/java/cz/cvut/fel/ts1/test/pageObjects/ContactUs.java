package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUs {
    WebDriver driver;

    @FindBy(className = "ContactUs")
    WebElement title;

    @FindBy(id = "reservation")
    WebElement reservaiton;

    public static ContactUs goTo(String baseUrl, WebDriver driver) {
        ContactUs contactUs = new ContactUs(driver);
        driver.get(baseUrl);
        return contactUs;
    }

    public ContactUs(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }

    public void reseravtionClicked(){
        reservaiton.click();
    }
}
