package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestReservation {
    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllReservation allReservation;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void TestLoginUserData(){
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("prokop@buben.cz");
        login.fillPassword("Test01");
        booked = login.submit();
        driver = booked.getDriver();

        assertTrue((navigationBar.getMyReservation().isDisplayed()));
        assertTrue((navigationBar.getAddcomment().isDisplayed()));
    }

    @Test
    @Order(2)
    public void TestCorrectReservation(){
        booked = new Booked(driver);

        assertEquals("2021-05-22 09:00:00", booked.getDayText());
        assertEquals("Prokop", booked.getNameText());
        assertEquals("prokop@buben.cz", booked.getEmailText());
        assertEquals("777777777", booked.getTelnumText());
    }

    @Test
    @Order(3)
    public void testLogOut(){
        booked = new Booked(driver);
        booked.logoutClick();
        driver = booked.getDriver();
        navigationBar = new NavigationBar(driver);

        assertTrue((navigationBar.getLoginButton().isDisplayed()));
    }

    @Test
    @Order(4)
    public void testReservationInSystem() throws InterruptedException {
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("admin@zwabarber.shop");
        login.fillPassword("ZWAadmin");
        login.submit();
        driver = login.getDriver();

        allReservation = new AllReservation(driver);
        boolean isSame = false;
        while(allReservation.getNextlinkLabel().isEnabled()){
            if(allReservation.getLabels().get(0).getText().equals("2021-05-22")){
                if (allReservation.getLabels().get(1).getText().equals("09:00:00")){
                    if (allReservation.getLabels().get(2).getText().equals("Prokop")){
                        isSame = true;
                        return;
                    }
                }
            } else {
                allReservation = allReservation.clickNext();
            }
        }

        assertTrue(isSame);
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
    }
}
