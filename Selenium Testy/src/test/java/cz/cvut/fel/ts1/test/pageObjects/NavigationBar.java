package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationBar {
    WebDriver driver;

    @FindBy(id = "index")
    WebElement homeButton;

    @FindBy(id = "galerry")
    WebElement galerryButton;

    @FindBy(id = "pricelist")
    WebElement pricelistButton;

    @FindBy(id = "chooseday")
    WebElement makeReservationButton;

    @FindBy(id = "aboutus")
    WebElement aboutusButton;

    @FindBy(id = "contactus")
    WebElement contactusButton;

    @FindBy(id = "login")
    WebElement loginButton;

    @FindBy(id = "allcomment")
    WebElement allcommentButton;

    @FindBy(id = "AllReservation")
    WebElement AllReservation;

    @FindBy(id = "booked")
    WebElement MyReservation;

    @FindBy(id = "addcomment")
    WebElement addcomment;

    public static NavigationBar goTo(String baseUrl, WebDriver driver) {
        NavigationBar navigationBar = new NavigationBar(driver);
        driver.get(baseUrl);
        return navigationBar;
    }

    public NavigationBar(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Login goToLoginPage(){
        loginButton.click();
        System.out.println("logging button clicked");
        return new Login(driver);
    }

    public ContactUs goToContactUsPage(){
        contactusButton.click();
        return new ContactUs(driver);
    }

    public AllComment goToAllCommentPage(){
        allcommentButton.click();
        return new AllComment(driver);
    }

    public AboutUs goToAboutUsPage(){
        aboutusButton.click();
        return new AboutUs(driver);
    }

    public ChooseDay goToReservationPage(){
        makeReservationButton.click();
        return new ChooseDay(driver);
    }

    public PriceList goToPriceListPage(){
        pricelistButton.click();
        return new PriceList(driver);
    }

    public Gallery goToGalleryPage(){
        galerryButton.click();
        return new Gallery(driver);
    }

    public HomePage goToHomePage(){
        homeButton.click();
        return new HomePage(driver);
    }

    public AddComment goToAddComment(){
        addcomment.click();
        return new AddComment(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getHomeButton() {
        return homeButton;
    }

    public WebElement getGalerryButton() {
        return galerryButton;
    }

    public WebElement getPricelistButton() {
        return pricelistButton;
    }

    public WebElement getMakeReservationButton() {
        return makeReservationButton;
    }

    public WebElement getAboutusButton() {
        return aboutusButton;
    }

    public WebElement getContactusButton() {
        return contactusButton;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public WebElement getAllcommentButton() {
        return allcommentButton;
    }

    public WebElement getAllReservation() {
        return AllReservation;
    }

    public WebElement getMyReservation() {
        return MyReservation;
    }

    public WebElement getAddcomment() {
        return addcomment;
    }
}


