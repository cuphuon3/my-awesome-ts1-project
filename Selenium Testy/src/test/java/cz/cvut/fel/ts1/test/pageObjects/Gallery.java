package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Gallery {
    WebDriver driver;

    @FindBy(className = "container")
    WebElement container;

    @FindBy(className = "pictures")
    WebElement pictures;

    @FindBy(className = "Gallery")
    WebElement title;

    @FindBy(css = ".pictures img")
    WebElement picture;

    @FindBy(id = ".zoomIn")
    WebElement pictureBig;

    public static Gallery goTo(String baseUrl, WebDriver driver) {
        Gallery gallery = new Gallery(driver);
        driver.get(baseUrl);
        return gallery;
    }

    public Gallery(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getContainer() {
        return container;
    }

    public WebElement getPictures() {
        return pictures;
    }

    public WebElement getPicture() {
        return picture;
    }

    public WebElement getPictureBig() {
        return pictureBig;
    }

    public void clickedOnPicture(){
        picture.click();
    }

    public WebElement getTitle() {
        return title;
    }
}
