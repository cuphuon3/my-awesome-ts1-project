package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AllReservation {
    WebDriver driver;

    @FindBy(className = "AllReservation")
    WebElement title;

    @FindBy(className = "container")
    WebElement container;

    @FindBy(className = "windows")
    WebElement windows;

    @FindBy(css = ".prevlink a")
    WebElement prevlink;

    @FindBy(css = ".nextlink a")
    WebElement nextlink;

    @FindBy(className = "nextlink")
    WebElement nextlinkLabel;

    @FindBy(className = "prevlink")
    WebElement prevlinkLabel;

    @FindBy(id = "serviceservice")
    WebElement dropList;

    @FindBy(css = ".windows .day")
    List<WebElement> labels;

    public static AllReservation goTo(String baseUrl, WebDriver driver) {
        AllReservation allReservation = new AllReservation(driver);
        driver.get(baseUrl);
        return allReservation;
    }

    public AllReservation(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getPrevlinkLabel() {
        return prevlinkLabel;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getContainer() {
        return container;
    }

    public WebElement getWindows() {
        return windows;
    }

    public WebElement getPrevlink() {
        return prevlink;
    }

    public WebElement getNextlink() {
        return nextlink;
    }

    public WebElement getDropList() {
        return dropList;
    }

    public AllReservation clickNext(){
        nextlink.click();
        return new AllReservation(driver);
    }

    public AllReservation clickPrev(){
        prevlink.click();
        return new AllReservation(driver);
    }

    public List<WebElement> getLabels() {
        return labels;
    }

    public WebElement getNextlinkLabel() {
        return nextlinkLabel;
    }
}