package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestLoggedUser {
    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void TestFormForLogin() throws InterruptedException {
        homePage = new HomePage(driver);
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        driver = login.getDriver();
        assertTrue(login.getForm().isDisplayed());
    }

    @Test
    @Order(2)
    public void TestInvalidData() throws InterruptedException {
        login = new Login(driver);
        login.fillEmail(" ");
        login.fillPassword(" ");
        login.submit();
        assertEquals(driver.getCurrentUrl(), login.getDriver().getCurrentUrl());
    }

    @Test
    @Order(3)
    public void TestValidData(){
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("prokop@buben.cz");
        login.fillPassword("Test01");
        booked = login.submit();

        assertTrue((navigationBar.getMyReservation().isDisplayed()));
        assertTrue((navigationBar.getAddcomment().isDisplayed()));

        assertEquals("2021-05-22 09:00:00", booked.getDayText());
        assertEquals("Prokop", booked.getNameText());
        assertEquals("prokop@buben.cz", booked.getEmailText());
        assertEquals("777777777", booked.getTelnumText());
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
    }
}
