package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AboutUs {
    WebDriver driver;

    @FindBy(className = "AboutUs")
    WebElement title;

    public static AboutUs goTo(String baseUrl, WebDriver driver) {
        AboutUs aboutUs = new AboutUs(driver);
        driver.get(baseUrl);
        return aboutUs;
    }

    public AboutUs(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }
}
