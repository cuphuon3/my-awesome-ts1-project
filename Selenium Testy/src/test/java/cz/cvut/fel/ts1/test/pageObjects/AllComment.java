package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class AllComment {
    WebDriver driver;

    @FindBy(className = "AllReservation")
    WebElement title;

    @FindBy(className = "container")
    WebElement container;

    @FindBy(className = "windows")
    WebElement windows;

    @FindBy(css = ".prevlink a")
    WebElement prevlink;

    @FindBy(css = ".nextlink a")
    WebElement nextlink;

    @FindBy(id = "service")
    WebElement dropList;

    @FindBy(className = "nextlink")
    WebElement nextlinkLabel;

    @FindBy(className = "prevlink")
    WebElement prevlinkLabel;

    @FindBy(css = ".windows .day")
    List<WebElement> labels;

    @FindBy(id = "filter")
    WebElement filter;



    public static AllComment goTo(String baseUrl, WebDriver driver) {
        AllComment allComment = new AllComment(driver);
        driver.get(baseUrl);
        return allComment;
    }

    public AllComment(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getContainer() {
        return container;
    }

    public WebElement getWindows() {
        return windows;
    }

    public WebElement getPrevlink() {
        return prevlink;
    }

    public WebElement getNextlink() {
        return nextlink;
    }

    public WebElement getDropList() {
        return dropList;
    }

    public WebElement getPrevlinkLabel() {
        return prevlinkLabel;
    }

    public WebElement getFilter() {
        return filter;
    }

    public void filter(String service){
        dropList.sendKeys(service);
    }

    public AllComment clickFilter(){
        filter.click();
        return new AllComment(driver);
    }

    public AllComment clickNext(){
        nextlink.click();
        return new AllComment(driver);
    }

    public AllComment clickPrev(){
        prevlink.click();
        return new AllComment(driver);
    }

    public List<WebElement> getLabels() {
        return labels;
    }

    public WebElement getNextlinkLabel() {
        return nextlinkLabel;
    }
}
