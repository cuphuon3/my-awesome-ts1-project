package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Booked {
    WebDriver driver;

    @FindBy(className = "container")
    WebElement container;

    @FindBy(id = "day")
    WebElement day;

    @FindBy(id = "name")
    WebElement name;

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "telnum")
    WebElement telnum;

    @FindBy(css = "#day p")
    WebElement dayText;

    @FindBy(css = "#name p")
    WebElement nameText;

    @FindBy(css = "#email p")
    WebElement emailText;

    @FindBy(css = "#telnum p")
    WebElement telnumText;

    @FindBy(className = "EditkButton")
    WebElement editButton;

    @FindBy(className = "DeleteButton")
    WebElement deleteButton;

    @FindBy(className = "LogOut")
    WebElement logout;

    public static Booked goTo(String baseUrl, WebDriver driver) {
        Booked booked = new Booked(driver);
        driver.get(baseUrl);
        return booked;
    }

    public Booked(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getContainer() {
        return container;
    }

    public WebElement getDay() {
        return day;
    }

    public WebElement getName() {
        return name;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getTelnum() {
        return telnum;
    }

    public String getDayText() {
        return dayText.getText();
    }

    public String getNameText() {
        return nameText.getText();
    }

    public String getEmailText() {
        return emailText.getText();
    }

    public String getTelnumText() {
        return telnumText.getText();
    }

    public WebElement getEditButton() {
        return editButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getLogout() {
        return logout;
    }

    public ChooseDay clickEdit(){
        editButton.click();
        return new ChooseDay(driver);
    }

    public void deleteClick(){
        deleteButton.click();
    }

    public void logoutClick(){
        logout.click();
    }
}
