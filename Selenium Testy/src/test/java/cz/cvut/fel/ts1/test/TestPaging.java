package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestPaging {
    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;
    private AllReservation allReservation;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void TestingPagingAllReservation(){
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("admin@zwabarber.shop");
        login.fillPassword("ZWAadmin");
        login.submit();
        driver = login.getDriver();

        allReservation = new AllReservation(driver);
        String expectedDay = allReservation.getLabels().get(0).getText();
        String expectedTime = allReservation.getLabels().get(1).getText();
        String expectedName = allReservation.getLabels().get(2).getText();

        while(allReservation.getNextlinkLabel().isEnabled()){
            try {
                allReservation = allReservation.clickNext();
            }catch (Exception exception){
                while(true) {
                    try {
                        allReservation = allReservation.clickPrev();
                    } catch (Exception exception1) {
                        return;
                    }
                }
            }
        }

        String resultDay = allReservation.getLabels().get(0).getText();
        String resultTime = allReservation.getLabels().get(1).getText();
        String resultName = allReservation.getLabels().get(2).getText();

        assertEquals(expectedDay, resultDay);
        assertEquals(expectedTime, resultTime);
        assertEquals(expectedName, resultName);
    }

    @Test
    @Order(2)
    public void TestingPagingAllComment(){
        navigationBar = new NavigationBar(driver);
        allComment = navigationBar.goToAllCommentPage();

        allComment = new AllComment(driver);
        String expectedTime = allComment.getLabels().get(0).getText();
        String expectedDay = allComment.getLabels().get(1).getText();
        String expectedComment = allComment.getLabels().get(2).getText();

        JavascriptExecutor js = (JavascriptExecutor)driver;
        while(allComment.getNextlinkLabel().isEnabled()){
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                allComment = allComment.clickNext();
            }catch (Exception exception){
                while(true) {
                    try {
                        allComment = allComment.clickPrev();
                    } catch (Exception exception1) {
                        return;
                    }
                }
            }
        }

        String resultTime = allComment.getLabels().get(0).getText();
        String resultDay = allComment.getLabels().get(1).getText();
        String resultComment = allComment.getLabels().get(2).getText();

        assertEquals(expectedTime, resultDay);
        assertEquals(expectedDay, resultTime);
        assertEquals(expectedComment, resultComment);
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
    }
}
