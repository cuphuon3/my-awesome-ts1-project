package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class BookIt {
    WebDriver driver;

    @FindBy(className = "form")
    WebElement form;

    @FindBy(id = "Choosedday")
    WebElement date;

    @FindBy(id = "Time")
    WebElement TimeField;

    @FindBy(id = "Name")
    WebElement NameField;

    @FindBy(id = "Email")
    WebElement EmailField;

    @FindBy(id = "Telnumber")
    WebElement TelnumberField;

    @FindBy(id = "Password")
    WebElement PasswordField;

    @FindBy(id = "PasswordCheck")
    WebElement PasswordCheckField;

    @FindBy(id = "comment")
    WebElement commentField;

    @FindBy(id = "covidAccept")
    WebElement covidAcceptField;


    @FindBy(id = "buttons")
    WebElement buttonsField;

    @FindBy(id = "send")
    WebElement sendButton;

    @FindBy(css = "#buttons .BackButton")
    WebElement BackButton;

    @FindBy(id = "reset")
    WebElement resetButton;

    @FindBy(id = "nine")
    WebElement nineButton;
    @FindBy(id = "ten")
    WebElement tenButton;
    @FindBy(id = "eleven")
    WebElement elevenButton;
    @FindBy(id = "twelve")
    WebElement twelveButton;
    @FindBy(id = "thirteen")
    WebElement thirteenButton;
    @FindBy(id = "fourteen")
    WebElement fourteenButton;
    @FindBy(id = "fifteen")
    WebElement fifteenButton;
    @FindBy(id = "sixteen")
    WebElement sixteenButton;

    @FindBy(id = "CustomerName")
    WebElement CustomerName;

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "telnumber")
    WebElement telnumber;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(id = "passwordCheck")
    WebElement passwordCheck;

    @FindBy(id = "vzkaz")
    WebElement vzkaz;

    @FindBy(id = "ano")
    WebElement ano;

    @FindBy(id = "ne")
    WebElement ne;

    @FindBy(id = "errorName")
    WebElement errorName;

    @FindBy(id = "errorEmail")
    WebElement errorEmail;

    @FindBy(id = "errorTel")
    WebElement errorTel;

    @FindBy(id = "errorPass")
    WebElement errorPass;

    @FindBy(id = "errorPassCheck")
    WebElement errorPassCheck;

    public static BookIt goTo(String baseUrl, WebDriver driver) {
        BookIt bookIt = new BookIt(driver);
        driver.get(baseUrl);
        return bookIt;
    }

    public BookIt(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getForm() {
        return form;
    }

    public WebElement getDate() {
        return date;
    }

    public WebElement getTimeField() {
        return TimeField;
    }

    public WebElement getNameField() {
        return NameField;
    }

    public WebElement getEmailField() {
        return EmailField;
    }

    public WebElement getTelnumberField() {
        return TelnumberField;
    }

    public WebElement getPasswordField() {
        return PasswordField;
    }

    public WebElement getPasswordCheckField() {
        return PasswordCheckField;
    }

    public WebElement getCommentField() {
        return commentField;
    }

    public WebElement getCovidAcceptField() {
        return covidAcceptField;
    }

    public WebElement getButtonsField() {
        return buttonsField;
    }

    public WebElement getNineButton() {
        return nineButton;
    }

    public WebElement getTenButton() {
        return tenButton;
    }

    public WebElement getElevenButton() {
        return elevenButton;
    }

    public WebElement getTwelveButton() {
        return twelveButton;
    }

    public WebElement getThirteenButton() {
        return thirteenButton;
    }

    public WebElement getFourteenButton() {
        return fourteenButton;
    }

    public WebElement getFifteenButton() {
        return fifteenButton;
    }

    public WebElement getSixteenButton() {
        return sixteenButton;
    }

    public WebElement getAno() {
        return ano;
    }

    public WebElement getNe() {
        return ne;
    }

    public WebElement getErrorName() {
        return errorName;
    }

    public WebElement getErrorEmail() {
        return errorEmail;
    }

    public WebElement getErrorTel() {
        return errorTel;
    }


    public WebElement getErrorPass() {
        return errorPass;
    }

    public WebElement getErrorPassCheck() {
        return errorPassCheck;
    }

    public WebElement getSendButton() {
        return sendButton;
    }

    public WebElement getBackButton() {
        return BackButton;
    }

    public WebElement getResetButton() {
        return resetButton;
    }

    public void fillName(String name){
        CustomerName.clear();
         CustomerName.sendKeys(name);
     }

     public void fillEmail(String emailToFill){
        email.clear();
        email.sendKeys(emailToFill);
     }

     public void fillTelephone(String telephone){
        telnumber.clear();
         telnumber.sendKeys(telephone);
     }

     public void fillPassword(String passwordToFill){
        password.clear();
        password.sendKeys(passwordToFill);
     }

     public void fillPasswordAgain(String password){
        passwordCheck.clear();
        passwordCheck.sendKeys(password);
     }

     public void fillVzkaz(String vzkazToFill){
        vzkaz.clear();
         vzkaz.sendKeys(vzkazToFill);
     }

     public void clickOn(WebElement elementToClick){
        elementToClick.click();
     }

     public Booked send(){
        sendButton.click();
        return new Booked(driver);
     }

    public WebDriver getDriver() {
        return driver;
    }
}
