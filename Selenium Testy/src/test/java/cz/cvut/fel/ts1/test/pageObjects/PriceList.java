package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PriceList {
    WebDriver driver;

    @FindBy(className = "priceList")
    WebElement title;

    public static PriceList goTo(String baseUrl, WebDriver driver) {
        PriceList gallery = new PriceList(driver);
        driver.get(baseUrl);
        return gallery;
    }

    public PriceList(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }
}
