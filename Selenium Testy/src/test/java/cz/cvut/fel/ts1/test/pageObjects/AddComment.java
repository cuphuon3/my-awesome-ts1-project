package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddComment {
    WebDriver driver;

    @FindBy(className = "BookIT")
    WebElement title;

    @FindBy(className = "form")
    WebElement form;

    @FindBy(id = "Name")
    WebElement TitleField;

    @FindBy(id = "Email")
    WebElement serviseField;

    @FindBy(id = "comment")
    WebElement commentField;

    @FindBy(id = "buttons")
    WebElement buttonsField;

    @FindBy(id = "CustomerName")
    WebElement titleTofill;

    @FindBy(id = "service")
    WebElement serviceToChoose;

    @FindBy(id = "vzkaz")
    WebElement vzkazToFill;

    @FindBy(id = "reset")
    WebElement reset;

    @FindBy(id = "add")
    WebElement add;

    public static AddComment goTo(String baseUrl, WebDriver driver) {
        AddComment addComment = new AddComment(driver);
        driver.get(baseUrl);
        return addComment;
    }

    public AddComment(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getForm() {
        return form;
    }

    public WebElement getTitleField() {
        return TitleField;
    }

    public WebElement getServiseField() {
        return serviseField;
    }

    public WebElement getCommentField() {
        return commentField;
    }

    public WebElement getButtonsField() {
        return buttonsField;
    }

    public WebElement getTitleTofill() {
        return titleTofill;
    }

    public WebElement getServiceToChoose() {
        return serviceToChoose;
    }

    public WebElement getVzkazToFill() {
        return vzkazToFill;
    }

    public WebElement getReset() {
        return reset;
    }

    public WebElement getAdd() {
        return add;
    }

    public void fillTitle(String title){
        titleTofill.sendKeys(title);
    }

    public void chooseService(String service){
        serviceToChoose.sendKeys(service);
    }

    public void fillComment(String comment){
        vzkazToFill.sendKeys(comment);
    }

    public AllComment clickAdd(){
        add.click();
        return new AllComment(driver);
    }

    public void clickReset(){
        reset.click();
    }
}
