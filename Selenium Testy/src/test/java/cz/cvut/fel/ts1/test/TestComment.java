package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestComment {
    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllReservation allReservation;
    private AllComment allComment;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void TestIsCommentInSystem(){
        navigationBar = new NavigationBar(driver);
        allComment = navigationBar.goToAllCommentPage();
        allComment.filter("Hair cutting");
        allComment = allComment.clickFilter();

        JavascriptExecutor js = (JavascriptExecutor)driver;

        allComment = new AllComment(driver);
        boolean isSame = false;
        while(allComment.getNextlinkLabel().isEnabled()){
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            if(allComment.getLabels().get(0).getText().equals("Ahoojda")){
                if (allComment.getLabels().get(1).getText().equals("2021-05-20 09:03:17")){
                    if (allComment.getLabels().get(2).getText().equals("Vsechno was allright")){
                        isSame = true;
                        return;
                    }
                }
            } else {
                allComment = allComment.clickNext();
            }
        }

        assertTrue(isSame);
    }

    @AfterEach
    public void cleanUp() {
        driver.close();
    }
}
