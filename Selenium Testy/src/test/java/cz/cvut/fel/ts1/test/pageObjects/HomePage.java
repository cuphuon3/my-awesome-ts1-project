package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    WebDriver driver;

    @FindBy(id = "reservationButton")
    WebElement reservationButton;

    public static HomePage goTo(String baseUrl, WebDriver driver) {
        HomePage homePage = new HomePage(driver);
        driver.get(baseUrl);
        return homePage;
    }

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void goToReservationPageMainButton(){
        reservationButton.click();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getReservationButton() {
        return reservationButton;
    }
}
