package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestDeleteReservation {

    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;
    private AllReservation allReservation;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void TestFormForLogin() throws InterruptedException {
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("tomas@jedno.cz");
        login.fillPassword("Test02");
        booked = login.submit();

        booked.deleteClick();
        navigationBar = new NavigationBar(driver);
        assertTrue((navigationBar.getLoginButton().isDisplayed()));
    }

    @Test
    @Order(2)
    public void TestLoginDeletedUser(){
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("tomas@jedno.cz");
        login.fillPassword("Test02");
        assertEquals("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/Login.php", driver.getCurrentUrl());
    }

    @Test
    @Order(3)
    public void testReservationNotInSystem() throws InterruptedException {
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("admin@zwabarber.shop");
        login.fillPassword("ZWAadmin");
        login.submit();
        driver = login.getDriver();

        allReservation = new AllReservation(driver);
        boolean isNotInSystem = true;
        while(allReservation.getNextlinkLabel().isEnabled()){
            if(allReservation.getLabels().get(0).getText().equals("2021-05-26")){
                if (allReservation.getLabels().get(1).getText().equals("09:00:00")){
                    if (allReservation.getLabels().get(2).getText().equals("Tomas")){
                        isNotInSystem = false;
                        return;
                    }
                }
            } else {
                try {
                    allReservation = allReservation.clickNext();
                }catch (Exception exception){
                    break;
                }
            }
        }

        assertTrue(isNotInSystem);
    }
}
