package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DuplicityAlert {
    WebDriver driver;

    @FindBy(className = "midButton")
    WebElement login;

    public static DuplicityAlert goTo(String baseUrl, WebDriver driver) {
        DuplicityAlert booked = new DuplicityAlert(driver);
        driver.get(baseUrl);
        return booked;
    }

    public DuplicityAlert(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getLogin() {
        return login;
    }
}
