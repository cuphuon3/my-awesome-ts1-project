package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestEditReservation {
    static WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;
    private AllReservation allReservation;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void TestFormForLogin() throws InterruptedException {
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
        login.fillEmail("mobilni@tester.cz");
        login.fillPassword("Test01");
        booked = login.submit();
        driver = booked.getDriver();

        assertTrue((navigationBar.getMyReservation().isDisplayed()));
        assertTrue((navigationBar.getAddcomment().isDisplayed()));

        assertEquals("2021-05-26 15:00:00", booked.getDayText());
        assertEquals("Mobilni", booked.getNameText());
        assertEquals("mobilni@tester.cz", booked.getEmailText());
        assertEquals("777444111", booked.getTelnumText());
    }

    @Test
    @Order(2)
    public void TestEditForm(){
        booked = new Booked(driver);
        chooseDay = booked.clickEdit();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2021-05-27");

        bookIt = chooseDay.submit();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        JavascriptExecutor js = (JavascriptExecutor)driver;

        assertTrue(bookIt.getTenButton().isEnabled());
        bookIt.clickOn(bookIt.getTenButton());

        bookIt.fillName("Mobilni");
        bookIt.fillEmail("mobilni@tester.cz");
        js.executeScript("window.scrollTo(0, 400)");
        bookIt.fillTelephone("777777777");
        bookIt.fillPassword("Test01");
        js.executeScript("window.scrollTo(0, 2000)");
        bookIt.fillPasswordAgain("Test01");
        bookIt.clickOn(bookIt.getAno());

        assertEquals("", bookIt.getErrorName().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorEmail().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorTel().getText());
        assertEquals("", bookIt.getErrorPass().getText());

        booked = bookIt.send();

        assertEquals("2021-05-27 10:00:00", booked.getDayText());
        assertEquals("Mobilni", booked.getNameText());
        assertEquals("mobilni@tester.cz", booked.getEmailText());
        assertEquals("777777777", booked.getTelnumText());
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
    }
}
