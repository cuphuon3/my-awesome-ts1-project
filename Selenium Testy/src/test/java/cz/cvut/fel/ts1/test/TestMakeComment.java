package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestMakeComment {

    static WebDriver driver;

    private HomePage homePage;
    static NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    static Login login;
    private Gallery gallery;
    private PriceList priceList;
    private AboutUs aboutUs;
    private ContactUs contactUs;
    private AddComment addComment;
    private AllComment allComment;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        navigationBar = new NavigationBar(driver);
        login = navigationBar.goToLoginPage();
    }

    @Test
    @Order(1)
    public void TestFormForAddComment() throws InterruptedException {
        login.fillEmail("prokop@buben.cz");
        login.fillPassword("Test01");
        booked = login.submit();
        navigationBar = new NavigationBar(booked.getDriver());
        addComment = navigationBar.goToAddComment();
        driver = addComment.getDriver();

        assertTrue(addComment.getTitle().isDisplayed());
        assertTrue(addComment.getForm().isDisplayed());
        assertTrue(addComment.getTitleField().isDisplayed());
        assertTrue(addComment.getServiseField().isDisplayed());
        assertTrue(addComment.getCommentField().isDisplayed());
        assertTrue(addComment.getButtonsField().isDisplayed());
    }

    @Test
    @Order(2)
    public void TestInvalidData(){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        addComment = new AddComment(driver);
        addComment.fillTitle("5");
        addComment.chooseService("Car");
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        addComment.fillComment("nic");
        addComment.clickAdd();

        assertTrue(addComment.getForm().isDisplayed());
    }

    @Test
    @Order(3)
    public void TestResetField(){
        addComment = new AddComment(driver);
        addComment.clickReset();

        assertTrue(addComment.getTitleTofill().getText().isEmpty());
        assertTrue(addComment.getVzkazToFill().getText().isEmpty());
    }

    @Test
    @Order(4)
    public void TestValidData(){
        addComment = new AddComment(driver);
        addComment.clickReset();
        addComment.fillTitle("Lorem");
        addComment.chooseService("Hair coloring");
        addComment.fillComment("Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, eaque. Deleniti, molestiae quis? Placeat ad enim aspernatur ea nemo corporis possimus dolores vitae nulla hic, autem iusto magni. Consectetur minima at eligendi sed facere quae impedit illum, commodi cumque! Nam sequi quo in sed ducimus deserunt, unde consectetur explicabo! Alias.");

        allComment = addComment.clickAdd();

        assertTrue(allComment.getContainer().isDisplayed());
    }

    @AfterAll
    public static void cleanUp() {
        driver.close();
    }
}
