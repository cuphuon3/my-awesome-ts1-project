package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Footer {
    WebDriver driver;

    @FindBy(id = "instagram")
    WebElement instagram;

    @FindBy(id = "emailG")
    WebElement google;

    public static Footer goTo(String baseUrl, WebDriver driver) {
        Footer footer = new Footer(driver);
        driver.get(baseUrl);
        return footer;
    }

    public Footer(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getInstagram() {
        return instagram;
    }

    public WebElement getGoogle() {
        return google;
    }

    public void clickOnIg(){
        instagram.click();
    }

    public void clickOnG(){
        google.click();
    }
}
