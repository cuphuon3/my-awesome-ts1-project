package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.test.pageObjects.*;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestMakeReservation {
    private WebDriver driver;

    private HomePage homePage;
    private NavigationBar navigationBar;
    private ChooseDay chooseDay;
    private BookIt bookIt;
    private Booked booked;
    private DuplicityAlert duplicityAlert;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Selenium/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://wa.toad.cz/~cuphuon3/Semestr%c3%a1lka_10.1/Page/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
        navigationBar = new NavigationBar(driver);
    }

    @Test
    @Order(1)
    public void testFormForValidationReturnMessageError() throws InterruptedException {
        navigationBar.goToReservationPage();
        chooseDay = navigationBar.goToReservationPage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2020-05-22");
        chooseDay.submit();

        assertEquals("PLEASE CHOOSE DAY IN INTERVAL 2021-05-21 TO 2021-05-27",chooseDay.getErrorLabel().getText());
    }

    @Test
    @Order(2)
    public void TestFormForRezervation(){
        navigationBar.goToReservationPage();
        chooseDay = navigationBar.goToReservationPage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2021-05-22");

        bookIt = chooseDay.submit();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        assertTrue(bookIt.getDate().getText().equals("2021-05-22"));
        assertTrue(bookIt.getTimeField().isDisplayed());
        assertTrue(bookIt.getEmailField().isDisplayed());
        assertTrue(bookIt.getNameField().isDisplayed());
        assertTrue(bookIt.getTelnumberField().isDisplayed());
        assertTrue(bookIt.getPasswordCheckField().isDisplayed());
        assertTrue(bookIt.getPasswordField().isDisplayed());
        assertTrue(bookIt.getCommentField().isDisplayed());
        assertTrue(bookIt.getCovidAcceptField().isDisplayed());
        assertTrue(bookIt.getButtonsField().isDisplayed());
    }

    @Test
    @Order(3)
    public void testFormValidationForRezervation() throws InterruptedException {
        navigationBar.goToReservationPage();
        chooseDay = navigationBar.goToReservationPage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2021-05-23");

        bookIt = chooseDay.submit();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        JavascriptExecutor js = (JavascriptExecutor)driver;
        bookIt.clickOn(bookIt.getNineButton());
        bookIt.fillName("Test01");
        bookIt.fillEmail("a");
        bookIt.fillTelephone("1");
        bookIt.fillPassword("1");
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        bookIt.fillPasswordAgain("1");

        assertEquals("INVALID NAME", bookIt.getErrorName().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("INVALID FORMAT", bookIt.getErrorEmail().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("INVALID TELEPHONE NUMBER, PLEASE TYPE AGAIN", bookIt.getErrorTel().getText());
        assertEquals("THE PASSWORD IS TO SHORT", bookIt.getErrorPass().getText());

        bookIt.clickOn(bookIt.getNineButton());
        bookIt.fillName("Petr");
        bookIt.fillEmail("petr@svetr.cz");
        js.executeScript("window.scrollTo(0, 400)");
        bookIt.fillTelephone("777777777");
        bookIt.fillPassword("Test01");
        js.executeScript("window.scrollTo(0, 2000)");
        bookIt.fillPasswordAgain("Test01");
        bookIt.clickOn(bookIt.getAno());

        assertEquals("", bookIt.getErrorName().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorEmail().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorTel().getText());
        assertEquals("", bookIt.getErrorPass().getText());

        booked = bookIt.send();

        assertEquals("2021-05-23 09:00:00", booked.getDayText());
        assertEquals("Petr", booked.getNameText());
        assertEquals("petr@svetr.cz", booked.getEmailText());
        assertEquals("777777777", booked.getTelnumText());
    }

    @Test
    @Order(4)
    public void testFormValidationForRezervation2() throws InterruptedException {
        navigationBar.goToReservationPage();
        chooseDay = navigationBar.goToReservationPage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2021-05-23");

        bookIt = chooseDay.submit();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        JavascriptExecutor js = (JavascriptExecutor)driver;

        assertTrue(bookIt.getNineButton().isEnabled());
        bookIt.clickOn(bookIt.getTenButton());

        bookIt.fillName("Progra");
        bookIt.fillEmail("progra@mator.cz");
        js.executeScript("window.scrollTo(0, 400)");
        bookIt.fillTelephone("777777777");
        bookIt.fillPassword("Test01");
        js.executeScript("window.scrollTo(0, 2000)");
        bookIt.fillPasswordAgain("Test01");
        bookIt.clickOn(bookIt.getAno());

        assertEquals("", bookIt.getErrorName().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorEmail().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorTel().getText());
        assertEquals("", bookIt.getErrorPass().getText());

        booked = bookIt.send();

        assertEquals("2021-05-23 10:00:00", booked.getDayText());
        assertEquals("Progra", booked.getNameText());
        assertEquals("progra@mator.cz", booked.getEmailText());
        assertEquals("777777777", booked.getTelnumText());
    }

    @Test
    @Order(5)
    public void testFormDuplicityEmailForRezervation() throws InterruptedException {
        navigationBar.goToReservationPage();
        chooseDay = navigationBar.goToReservationPage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        chooseDay.chooseDay("2021-05-23");

        bookIt = chooseDay.submit();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        JavascriptExecutor js = (JavascriptExecutor)driver;

        assertTrue(bookIt.getNineButton().isEnabled());
        bookIt.clickOn(bookIt.getElevenButton());

        bookIt.fillName("Progra");
        bookIt.fillEmail("progra@mator.cz");
        js.executeScript("window.scrollTo(0, 400)");
        bookIt.fillTelephone("777777777");
        bookIt.fillPassword("Test01");
        js.executeScript("window.scrollTo(0, 2000)");
        bookIt.fillPasswordAgain("Test01");
        bookIt.clickOn(bookIt.getAno());

        assertEquals("", bookIt.getErrorName().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorEmail().getText());
        js.executeScript("window.scrollTo(0, -400)");
        assertEquals("", bookIt.getErrorTel().getText());
        assertEquals("", bookIt.getErrorPass().getText());

        bookIt.send();
        duplicityAlert = new DuplicityAlert(bookIt.getDriver());

        assertTrue(duplicityAlert.getLogin().isDisplayed());
    }

    @AfterEach
    public void cleanUp() {
        driver.close();
    }
}
