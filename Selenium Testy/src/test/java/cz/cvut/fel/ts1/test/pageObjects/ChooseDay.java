package cz.cvut.fel.ts1.test.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ChooseDay {
    WebDriver driver;
    BookIt bookIt;

    @FindBy(css = ".container")
    WebElement homeButton;

    @FindBy(id = "choosedDay")
    WebElement dateField;

    @FindBy(id = "submitDay")
    WebElement nextButton;

    @FindBy(id = "error")
    WebElement errorLabel;

    public static ChooseDay goTo(String baseUrl, WebDriver driver) {
        ChooseDay chooseDay = new ChooseDay(driver);
        driver.get(baseUrl);
        return chooseDay;
    }

    public ChooseDay(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public BookIt submit(){
        nextButton.click();
        return new BookIt(driver);
    }

    public void chooseDay(String date){
        dateField.clear();
        dateField.sendKeys(date);
    }

    public WebElement getErrorLabel() {
        return errorLabel;
    }
}
